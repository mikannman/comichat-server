module RequestHelpers
  def sign_in(user)
    query = <<~QUERY
      mutation SignIn($email: String!, $password: String!) {
        signIn(input: {
          email: $email
          password: $password
        })
        {
          user {
            id
            name
            email
          }
          token
          success
        }
      }
    QUERY
    variables = {
      "email" => "#{user.email}",
      "password" => "password1",
    }

    post graphql_path,
         params: {
           query: query,
           variables: variables,
         },
         headers: {
           "Authorization" => "",
         }

    JSON.parse(response.body, symbolize_names: true)
  end

  def sign_out(token)
    query = <<~QUERY
      mutation signOut {
        signOut(input: {}) {
          success
          errors
        }
      }
    QUERY
    post graphql_path,
         params: {
           query: query,
         },
         headers: {
           "Authorization" => "Bearer #{token}",
         }

    JSON.parse(response.body, symbolize_names: true)
  end
end
