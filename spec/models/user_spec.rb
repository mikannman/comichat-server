require "rails_helper"

RSpec.describe User, type: :model do
  let(:user) { create(:user, name: "test_1") }
  let(:other_user) { create(:user, name: "test_2") }
  let(:manga) { create(:manga) }
  let(:answer) { create(:answer) }
  let(:reply) { create(:reply) }
  let(:review) { create(:review) }

  it "follows and unfollow a user" do
    expect(user.following?(other_user)).to be_falsy

    user.follow(other_user)
    expect(user.following?(other_user)).to be_truthy

    dup_other_user = user.active_relationships.find_by(followed_id: other_user.id).followed
    user.unfollow(dup_other_user)
    expect(user.following?(other_user)).to be_falsy
  end

  it "registers wanna read manga and unregister wanna read manga" do
    expect(user.wanna_read_manga?(manga)).to be_falsy

    user.register_wanna_read_manga(manga)
    expect(user.wanna_read_manga?(manga)).to be_truthy

    user.unregister_wanna_read_manga(manga)
    expect(user.wanna_read_manga?(manga)).to be_falsy
  end

  it "registers readed manga and unregister readed manga" do
    expect(user.readed_manga?(manga)).to be_falsy

    user.register_readed_manga(manga)
    expect(user.readed_manga?(manga)).to be_truthy

    user.unregister_readed_manga(manga)
    expect(user.readed_manga?(manga)).to be_falsy
  end

  it "likes answer and dislike answer" do
    expect(user.answer_liked?(answer)).to be_falsy

    user.likes_answer(answer)
    expect(user.answer_liked?(answer)).to be_truthy

    user.dislikes_answer(answer)
    expect(user.answer_liked?(answer)).to be_falsy
  end

  it "likes reply and dislike reply" do
    expect(user.reply_liked?(reply)).to be_falsy

    user.likes_reply(reply)
    expect(user.reply_liked?(reply)).to be_truthy

    user.dislikes_reply(reply)
    expect(user.reply_liked?(reply)).to be_falsy
  end

  it "likes review and dislike review" do
    expect(user.review_liked?(review)).to be_falsy

    user.likes_review(review)
    expect(user.review_liked?(review)).to be_truthy

    user.dislikes_review(review)
    expect(user.review_liked?(review)).to be_falsy
  end
end
