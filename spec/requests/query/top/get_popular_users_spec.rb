require "rails_helper"

RSpec.describe "Query::Top::GetPopularUsers", type: :request do
  describe "query get_popular_users" do
    let!(:user) { create(:user, :with_2_reviews) }
    let!(:user_2) { create(:user, :with_3_reviews) }
    let(:query) do
      <<~QUERY
        query GetPopularUsers {
          getPopularUsers {
            id
            name
            userId
            imageURL
          }
        }
      QUERY
    end

    it "returns correct ranking without current_user" do
      result = ServerSchema.execute(
        query,
        context: { current_user: { user: nil } },
      )

      expect(
        result.dig(
          "data", "getPopularUsers", 0, "id"
        )
      ).to eq(user_2.id.to_s)
      expect(
        result.dig(
          "data", "getPopularUsers", 1, "id"
        )
      ).to eq(user.id.to_s)
    end

    it "returns correct ranking with current_user" do
      result = ServerSchema.execute(
        query,
        context: { current_user: { user: user } },
      )

      expect(
        result.dig(
          "data", "getPopularUsers", 0, "id"
        )
      ).to eq(user_2.id.to_s)
      expect(
        result.dig(
          "data", "getPopularUsers"
        ).length
      ).to eq(1)
    end
  end
end
