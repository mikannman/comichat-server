require "rails_helper"

RSpec.describe "Query::Top::GetInterestingMangas", type: :request do
  describe "query get_interesting_mangas" do
    let(:user) { create(:user) }
    let!(:manga_1) do
      create(
        :manga,
        :with_3_goot_ratings,
        mid: "second"
      )
    end
    let!(:manga_2) do
      create(
        :manga,
        :with_3_goot_ratings,
        :with_3_normal_ratings,
        mid: "first"
      )
    end
    let!(:manga_3) do
      create(
        :manga,
        :with_3_bad_ratings,
        mid: "third",
      )
    end
    let(:query) do
      <<~QUERY
        query GetInterestingMangas($first: Int) {
          getInterestingMangas(first: $first) {
            edges {
              node {
                mid
                title
                reviews {
                  average
                }
              }
            }
          }
        }
      QUERY
    end

    it "returns correct ranking" do
      variables = {
        first: 3,
      }

      result = ServerSchema.execute(
        query,
        variables: variables,
        context: { current_user: { user: user } },
      )

      expect(
        result.dig(
          "data", "getInterestingMangas", "edges", 0, "node", "mid"
        )
      ).to eq(manga_2.mid)
      expect(
        result.dig(
          "data", "getInterestingMangas", "edges", 1, "node", "mid"
        )
      ).to eq(manga_1.mid)
      expect(
        result.dig(
          "data", "getInterestingMangas", "edges", 2, "node", "mid"
        )
      ).to eq(manga_3.mid)
    end
  end
end
