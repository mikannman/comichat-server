require "rails_helper"

RSpec.describe "Query::Manga::GetMangas", type: :request do
  describe "query get_manga" do
    let(:user) { create(:user) }
    let(:manga) { create(:manga, :with_two_reviews) }
    let(:query) do
      <<~QUERY
        query GetManga($mid: String!) {
          getManga(mid: $mid) {
            mid
            title
            synopsis
            datePublished
            publisher
            reviews {
              average
            }
            currentUserWannaRead {
              isExist
            }
            currentUserReaded {
              isExist
            }
          }
        }
      QUERY
    end

    context "when register wanna-read-manga" do
      it "returns true with register wanna-read-manga" do
        user.register_wanna_read_manga(manga)

        variables = {
          mid: manga.mid,
        }

        result = ServerSchema.execute(
          query,
          context: { current_user: { user: user } },
          variables: variables
        )

        expect(result.dig("data", "getManga", "currentUserWannaRead", "isExist")).to be_truthy
      end

      it "returns false without register wanna-read-manga" do
        variables = {
          "mid" => manga.mid,
        }

        result = ServerSchema.execute(
          query,
          context: { current_user: { user: user } },
          variables: variables
        )

        expect(result.dig("data", "getManga", "currentUserWannaRead", "isExist")).to be_falsy
      end
    end

    context "when register readed-manga" do
      it "returns true with register readed-manga" do
        user.register_readed_manga(manga)

        variables = {
          "mid" => manga.mid,
        }

        result = ServerSchema.execute(
          query,
          context: { current_user: { user: user } },
          variables: variables
        )

        expect(result.dig("data", "getManga", "currentUserReaded", "isExist")).to be_truthy
      end

      it "returns false without register readed-manga" do
        variables = {
          "mid" => manga.mid,
        }

        result = ServerSchema.execute(
          query,
          context: { current_user: { user: user } },
          variables: variables
        )

        expect(result.dig("data", "getManga", "currentUserReaded", "isExist")).to be_falsy
      end
    end

    it "returns 1 reviews average" do
      variables = {
        "mid" => manga.mid,
      }

      result = ServerSchema.execute(
        query,
        context: { current_user: { user: user } },
        variables: variables
      )

      expect(result.dig("data", "getManga", "reviews", "average")).to eq(1.5)
    end
  end
end
