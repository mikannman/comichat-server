require "rails_helper"

RSpec.describe "Query::User::GetRecommendedUsers", type: :request do
  describe "query get_recommended_users" do
    let!(:user) { create(:user, name: "テスト") }
    let!(:recommended_user) { create(:user, name: "リコメンド") }
    let!(:other_user) { create(:user, name: "アーザー") }
    let!(:other_user_2) { create(:user, name: "アーザー2") }
    let!(:users) { create_list(:user, 3) }
    let!(:favorite_manga) { create_list(:manga, 5) }
    let(:query) do
      <<~QUERY
        query GetRecommendedUsers {
          getRecommendedUsers {
            id
            name
            userId
            imageURL
          }
        }
      QUERY
    end

    it "returns correct users" do
      favorite_manga.each do |manga|
        user.register_readed_manga(manga)
      end
      favorite_manga.each do |manga|
        recommended_user.register_readed_manga(manga)
      end

      result = ServerSchema.execute(
        query,
        context: { current_user: { user: user } },
      )

      expect(result.dig(
        "data", "getRecommendedUsers", 0, "name"
      )).to eq(recommended_user.name)
      expect(result.dig(
        "data", "getRecommendedUsers", 1, "name"
      )).to eq(other_user.name)
      expect(result.dig(
        "data", "getRecommendedUsers"
      ).length).to eq(5)
    end
  end
end
