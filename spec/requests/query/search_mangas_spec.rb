require "rails_helper"

RSpec.describe "Query::SearchMangas", type: :request do
  describe "query search_manga" do
    before do
      11.times do |n|
        Manga.create(
          mid: "test_#{n}",
          title: "test_#{n}"
        )
        Kana.create(
          mid: "test_#{n}",
          kana: "test_#{n}"
        )
      end
      Manga.create(mid: "もっと わんこそば！", title: "もっと わんこそば！")
      Kana.create(mid: "もっと わんこそば！", kana: "もっと わんこそば！")
      Kana.create(mid: "もっと わんこそば！", kana: "もっと わんこsoba！")

      Manga.create(mid: "one piece", title: "one piece")
      Kana.create(mid: "one piece", kana: "わんぴーす")
      Kana.create(mid: "one piece", kana: "one piece")

      Manga.create(mid: "かわいいわん！", title: "かわいいわん！")
      Kana.create(mid: "かわいいわん！", kana: "かわいいわん！")
      Kana.create(mid: "かわいいわん！", kana: "かわいいone！")
    end

    def urlsafe_encode64(bin)
      str = Base64.strict_encode64(bin).tr("+/", "-_")
      str = str.delete("=")
      str
    end

    let(:query) do
      <<~QUERY
      query SearchManga($word: String!, $first: Int, $after: String) {
        searchManga(word: $word, first: $first, after: $after) {
          edges {
            cursor
            node {
              mid
              title
              datePublished
              publisher
            }
          }
          pageInfo{
            endCursor
            hasNextPage
            startCursor
            hasPreviousPage
          }
          totalCount
        }
      }
      QUERY
    end

    it "returns correct total count" do
      variables = {
        word: "test",
      }

      result = ServerSchema.execute(
        query,
        variables: variables
      )

      expect(result.dig("data", "searchManga", "totalCount")).to eq(11)
    end

    it "returns correct cursor encoded base64" do
      variables = {
        word: "test",
        first: 3,
        after: urlsafe_encode64("10"),
      }

      result = ServerSchema.execute(
        query,
        variables: variables
      )

      expect(result.dig("data", "searchManga", "edges", 0, "cursor")).to eq(urlsafe_encode64("11"))
    end

    it "returns correct cursor by paginate" do
      page = 2
      per_page = 3
      after = (page - 1) * per_page - 1

      variables = {
        word: "test",
        first: per_page,
        after: urlsafe_encode64(after.to_s),
      }

      result = ServerSchema.execute(
        query,
        variables: variables
      )

      expect(result.dig("data", "searchManga", "edges", 0, "cursor")).to eq(urlsafe_encode64("3"))
    end

    it "returns correct title by paginate" do
      per_page = 3

      variables = {
        word: "わん",
        first: per_page,
      }

      result = ServerSchema.execute(
        query,
        variables: variables
      )

      expect(result.dig("data", "searchManga", "edges", 0, "node", "title")).to eq("one piece")
    end
  end
end
