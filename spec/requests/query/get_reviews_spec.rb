require "rails_helper"

RSpec.describe "Query::GetReviews", type: :request do
  describe "query get_review" do
    let!(:manga) { create(:manga, :with_two_reviews) }
    let(:query) do
      <<~QUERY
      query GetReviews($mid: String!, $first: Int) {
        getReviews(mid: $mid, first: $first) {
          edges {
            cursor
            node {
              star
              comment
              user {
                name
                imageURL
              }
            }
          }
          pageInfo{
            endCursor
            hasNextPage
            startCursor
            hasPreviousPage
          }
          totalCount
        }
      }
      QUERY
    end

    it "returns correct paramaters" do
      variables = {
        mid: manga.mid,
        first: 3,
      }

      result = ServerSchema.execute(
        query,
        variables: variables
      )

      expect(
        result.dig("data", "getReviews", "edges", 0, "node", "comment")
      ).to eq(manga.reviews[1].comment)
      expect(
        result.dig("data", "getReviews", "edges", 1, "node", "comment")
      ).to eq(manga.reviews[0].comment)
      expect(result.dig("data", "getReviews", "edges").length).to eq(2)
    end
  end
end
