require "rails_helper"

RSpec.describe "Query::Notification::GetNotifications", type: :request do
  describe "query get_notifications" do
    let(:query) do
      <<~QUERY
        query GetNotifications($first: Int, $after: String) {
          getNotifications(first: $first, after: $after) {
            edges {
              node {
                id
                visitor {
                  id
                  name
                  imageURL
                }
                question {
                  id
                  content
                }
                answer {
                  content
                  questionId
                }
                reply {
                  content
                  answer {
                    questionId
                  }
                }
                review {
                  comment
                  mid
                }
                action
              }
            }
            totalCount
          }
        }
      QUERY
    end

    it "does not return error" do
      variables = {
        first: 5,
      }

      result = ServerSchema.execute(
        query,
        variables: variables,
        context: { current_user: { user: nil } },
      )

      expect(result.dig("data", "getNotifications", "totalCount")).to eq(0)
    end
  end
end
