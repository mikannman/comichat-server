require "rails_helper"

RSpec.describe "Query::GetPopularKeywords", type: :request do
  describe "query get_popular_keywords" do
    before do
      5.times do
        PopularKeyword.create(
          word: "one"
        )
      end
      3.times do
        PopularKeyword.create(
          word: "two"
        )
      end
      2.times do
        PopularKeyword.create(
          word: "three"
        )
      end
    end

    let(:query) do
      <<~QUERY
        query GetPopularKeywords {
          getPopularKeywords {
            word
          }
        }
      QUERY
    end

    it "returns correct runk" do
      result = ServerSchema.execute(
        query,
      )

      expect(result.dig("data", "getPopularKeywords", 0, "word")).to eq("one")
      expect(result.dig("data", "getPopularKeywords", 1, "word")).to eq("two")
      expect(result.dig("data", "getPopularKeywords", 2, "word")).to eq("three")
    end
  end
end
