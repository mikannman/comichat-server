require "rails_helper"

RSpec.describe "Query::Taught::GetQuestionAnswers", type: :request do
  describe "query get_question_answers" do
    let!(:answer) { create(:answer, :with_10_replies, :with_10_likes) }

    let(:query) do
      <<~QUERY
      query GetQuestionAnswers($id: ID!, $first: Int, $after: String) {
        getQuestionAnswers(id: $id, first: $first, after: $after) {
          edges {
            node {
              id
              content
              updatedAt
              replies {
                count
              }
              likeAnswers {
                count
              }
              user {
                id
                name
                imageURL
              }
            }
          }
          pageInfo{
            hasNextPage
            hasPreviousPage
          }
          totalCount
        }
      }
      QUERY
    end

    it "returns correct paramaters" do
      variables = {
        id: answer.question_id,
        first: 3,
      }

      result = ServerSchema.execute(
        query,
        variables: variables,
        context: { current_user: { user: answer.user } },
      )

      expect(
        result.dig(
          "data", "getQuestionAnswers", "edges", 0, "node", "replies",
          "count"
        )
      ).to eq(10)
      expect(
        result.dig(
          "data", "getQuestionAnswers", "edges", 0, "node", "likeAnswers",
          "count"
        )
      ).to eq(10)
    end
  end
end
