require "rails_helper"

RSpec.describe "Query::Taught::GetAnsweredLists", type: :request do
  describe "query get_answered_list" do
    let!(:answered_question) { create(:answered_question, :with_asked_user) }
    let!(:question) { create(:question) }

    let(:query) do
      <<~QUERY
      query GetAnsweredList($id: ID!, $first: Int) {
        getAnsweredList(id: $id, first: $first) {
          edges {
            node {
              id
              readingMangaMid
              readedMangaMid
              content
              all
              askedUser {
                id
                name
                imageURL
              }
            }
          }
          totalCount
        }
      }
      QUERY
    end

    it "returns correct paramaters" do
      variables = {
        id: answered_question.ask_user.id,
        first: 3,
      }

      result = ServerSchema.execute(
        query,
        variables: variables
      )

      expect(
        result.dig(
          "data", "getAnsweredList", "edges", 0, "node", "content"
        )
      ).to eq(answered_question.content)
      expect(
        result.dig(
          "data", "getAnsweredList", "edges", 0, "node", "askedUser",
          "name"
        )
      ).to eq(answered_question.asked_user.name)
      expect(result.dig("data", "getAnsweredList", "totalCount")).to eq(1)
    end
  end
end
