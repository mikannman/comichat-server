require "rails_helper"

RSpec.describe "Query::Taught::GetAskedLists", type: :request do
  describe "query get_asked_list" do
    let(:query) do
      <<~QUERY
      query GetAskedList($id: ID!, $first: Int) {
        getAskedList(id: $id, first: $first) {
          edges {
            node {
              id
              readingMangaMid
              readedMangaMid
              content
              all
              askedUser {
                id
                name
                imageURL
              }
            }
          }
          totalCount
        }
      }
      QUERY
    end

    context "when without asked_user, answers" do
      let!(:question) { create(:question) }
      let!(:question_2) { create(:question, ask_user_id: question.ask_user.id) }
      let!(:question_3) { create(:question, ask_user_id: question.ask_user.id) }

      it "returns correct paramaters" do
        variables = {
          id: question.ask_user.id,
          first: 2,
        }

        result = ServerSchema.execute(
          query,
          variables: variables
        )

        expect(
          result.dig(
            "data", "getAskedList", "edges", 0, "node", "content"
          )
        ).to eq(question_3.content)
        expect(
          result.dig(
            "data", "getAskedList", "edges", 1, "node", "content"
          )
        ).to eq(question_2.content)
        expect(
          result.dig(
            "data", "getAskedList", "edges"
          ).length
        ).to eq(2)
        expect(
          result.dig(
            "data", "getAskedList", "edges", 0, "node", "askedUser"
          )
        ).to eq(nil)

        expect(result.dig("data", "getAskedList", "totalCount")).to eq(3)
      end
    end

    context "when with asked_user, answers" do
      let!(:question) { create(:question, :with_asked_user, :with_10_answers) }

      it "returns correct paramaters" do
        variables = {
          id: question.ask_user.id,
          first: 2,
        }

        result = ServerSchema.execute(
          query,
          variables: variables
        )

        expect(
          result.dig(
            "data", "getAskedList", "edges", 0, "node", "content"
          )
        ).to eq(question.content)
        expect(
          result.dig(
            "data", "getAskedList", "edges", 0, "node", "askedUser",
            "name"
          )
        ).to eq(question.asked_user.name)
        expect(result.dig("data", "getAskedList", "totalCount")).to eq(1)
      end
    end
  end
end
