require "rails_helper"

RSpec.describe "Mutation::User::DeleteUsers", type: :request do
  describe "mutation delete_user" do
    let(:user) { create(:user) }
    let(:query) do
      <<~QUERY
        mutation deleteUser {
          deleteUser(input: {}) {
            success
          }
        }
      QUERY
    end

    it "returns success" do
      result = ServerSchema.execute(
        query,
        context: { current_user: { user: user } },
      )

      expect(result.dig("data", "deleteUser", "success")).to be_truthy
    end
  end
end
