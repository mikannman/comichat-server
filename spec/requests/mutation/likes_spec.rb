require "rails_helper"

RSpec.describe "Mutation::Likes", type: :request do
  describe "mutation likes" do
    let!(:user) { create(:user) }
    let!(:answer) { create(:answer) }
    let!(:reply) { create(:reply) }
    let!(:review) { create(:review) }
    let(:query) do
      <<~QUERY
        mutation Likes($id: ID!, $type: String!) {
          likes(input: {
            id: $id,
            type: $type
          })
          {
            success
          }
        }
      QUERY
    end

    context "when type is Answer" do
      it "returns success" do
        expect(user.answer_liked?(answer)).to be_falsy

        variables = {
          id: answer.id.to_s,
          type: "Answer",
        }

        result = ServerSchema.execute(
          query,
          context: { current_user: { user: user } },
          variables: variables
        )

        expect(result.dig("data", "likes", "success")).to be_truthy
        expect(user.answer_liked?(answer)).to be_truthy
      end
    end

    context "when type is Reply" do
      it "returns success" do
        expect(user.reply_liked?(reply)).to be_falsy

        variables = {
          id: reply.id,
          type: "Reply",
        }

        result = ServerSchema.execute(
          query,
          context: { current_user: { user: user } },
          variables: variables
        )

        expect(result.dig("data", "likes", "success")).to be_truthy
        expect(user.reply_liked?(reply)).to be_truthy
      end
    end

    context "when type is Review" do
      it "returns success" do
        expect(user.review_liked?(review)).to be_falsy

        variables = {
          id: review.id,
          type: "Review",
        }

        result = ServerSchema.execute(
          query,
          context: { current_user: { user: user } },
          variables: variables
        )

        expect(result.dig("data", "likes", "success")).to be_truthy
        expect(user.review_liked?(review)).to be_truthy
      end
    end

    context "when type is invalid" do
      it "returns error" do
        variables = {
          id: answer.id,
          type: "hoge",
        }

        result = ServerSchema.execute(
          query,
          context: { current_user: { user: user } },
          variables: variables
        )

        expect(result.dig("errors", 0, "message")).to eq("無効なタイプです")
      end
    end
  end
end
