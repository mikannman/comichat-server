require "rails_helper"

RSpec.describe "Mutation::SignUpOrIns", type: :request do
  let(:query) do
    <<~QUERY
    mutation SignUpOrIn(
      $name: String!,
      $email: String!,
      $imageURL: String,
      $provider: String!
      ) {
      signUpOrIn(input: {
        name: $name
        email: $email
        imageURL: $imageURL
        provider: $provider
      })
      {
        user {
          id
          name
          email
          imageURL
        }
        success
      }
    }
    QUERY
  end

  it "can not sign up user if auth-token is empty" do
    variables = {
      name: "test",
      email: "test@gmail.com",
      imageURL: "",
      provider: "password",
    }

    post graphql_path,
         params: {
           query: query,
           variables: variables.to_json,
         },
         headers: {
           "Authorization" => "",
         }

    result = JSON.parse(response.body, symbolize_names: true)

    expect(result.dig(:errors, 0, :message)).to eq(
      "ログインに失敗しました。しばらく時間をおいてもう一度お願いします。"
    )
  end

  it "can not sign up user if auth-token is invalid" do
    variables = {
      name: "test",
      email: "test@gmail.com",
      imageURL: "",
      provider: "password",
    }

    post graphql_path,
         params: {
           query: query,
           variables: variables.to_json,
         },
         headers: {
           "Authorization" => "test",
         }

    result = JSON.parse(response.body, symbolize_names: true)

    expect(result.dig(:errors, 0, :message)).to eq(
      "ログインに失敗しました。しばらく時間をおいてもう一度お願いします。"
    )
  end
end
