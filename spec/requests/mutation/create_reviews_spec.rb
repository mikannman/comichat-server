require "rails_helper"

RSpec.describe "Mutation::CreateReviews", type: :request do
  describe "mutation create_review" do
    let(:user) { create(:user) }
    let!(:manga) { create(:manga) }
    let(:query) do
      <<~QUERY
        mutation createReview($mid: String!, $star: Int!, $comment: String) {
          createReview(input: {
            mid: $mid,
            star: $star,
            comment: $comment
          })
          {
            review {
              star
              comment
              user {
                id
                name
                imageURL
              }
            }
          }
        }
      QUERY
    end

    it "returns correct paramaters" do
      variables = {
        mid: manga.mid,
        star: 2,
        comment: "comment",
      }

      result = ServerSchema.execute(
        query,
        context: { current_user: { user: user } },
        variables: variables
      )

      expect(result.dig("data", "createReview", "review", "comment")).to eq("comment")
      expect(result.dig("data", "createReview", "review", "star")).to eq(2)
      expect(result.dig("data", "createReview", "review", "user", "id")).to eq(user.id.to_s)
    end
  end
end
