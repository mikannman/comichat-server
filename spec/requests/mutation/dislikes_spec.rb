require "rails_helper"

RSpec.describe "Mutation::Dislikes", type: :request do
  describe "mutation dislikes" do
    let(:user) { create(:user) }
    let!(:answer) { create(:answer) }
    let!(:reply) { create(:reply) }
    let!(:review) { create(:review) }
    let(:query) do
      <<~QUERY
        mutation Dislikes($id: ID!, $type: String!) {
          dislikes(input: {
            id: $id,
            type: $type
          })
          {
            success
          }
        }
      QUERY
    end

    context "when type is Answer" do
      it "returns success" do
        user.likes_answer(answer)
        expect(user.answer_liked?(answer)).to be_truthy

        variables = {
          id: answer.id,
          type: "Answer",
        }

        result = ServerSchema.execute(
          query,
          context: { current_user: { user: user } },
          variables: variables
        )

        expect(result.dig("data", "dislikes", "success")).to be_truthy
        expect(user.answer_liked?(answer)).to be_falsy
      end
    end

    context "when type is Reply" do
      it "returns success" do
        user.likes_reply(reply)

        expect(user.reply_liked?(reply)).to be_truthy

        variables = {
          id: reply.id,
          type: "Reply",
        }

        result = ServerSchema.execute(
          query,
          context: { current_user: { user: user } },
          variables: variables
        )

        expect(result.dig("data", "dislikes", "success")).to be_truthy
        expect(user.reply_liked?(reply)).to be_falsy
      end
    end

    context "when type is Review" do
      it "returns success" do
        user.likes_review(review)

        expect(user.review_liked?(review)).to be_truthy

        variables = {
          id: review.id,
          type: "Review",
        }

        result = ServerSchema.execute(
          query,
          context: { current_user: { user: user } },
          variables: variables
        )

        expect(result.dig("data", "dislikes", "success")).to be_truthy
        expect(user.review_liked?(review)).to be_falsy
      end
    end

    context "when type is invalid" do
      it "returns error" do
        variables = {
          id: answer.id,
          type: "hoge",
        }

        result = ServerSchema.execute(
          query,
          context: { current_user: { user: user } },
          variables: variables
        )

        expect(result.dig("errors", 0, "message")).to eq("無効なタイプです")
      end
    end
  end
end
