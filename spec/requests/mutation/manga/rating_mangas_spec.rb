require "rails_helper"

RSpec.describe "Mutation::Manga::RatingMangas", type: :request do
  describe "mutation rating_mangas" do
    let(:user) { create(:user) }
    let(:manga) { create(:manga) }
    let(:query) do
      <<~QUERY
        mutation RatingManga($mid: String!, $rate: String!) {
          ratingManga(input: {
            mid: $mid,
            rate: $rate
          })
          {
            rating {
              id
              userId
              mid
              rate
            }
          }
        }
      QUERY
    end

    it "returns correct paramaters" do
      variables = {
        mid: manga.mid,
        rate: "good",
      }

      result = ServerSchema.execute(
        query,
        variables: variables,
        context: { current_user: { user: user } },
      )

      expect(result.dig("data", "ratingManga", "rating", "userId")).to eq(user.id)
      expect(result.dig("data", "ratingManga", "rating", "mid")).to eq(manga.mid)
      expect(result.dig("data", "ratingManga", "rating", "rate")).to eq("good")

      variables_2 = {
        mid: manga.mid,
        rate: "bad",
      }

      result_2 = ServerSchema.execute(
        query,
        variables: variables_2,
        context: { current_user: { user: user } },
      )

      expect(result_2.dig("data", "ratingManga", "rating", "userId")).to eq(user.id)
      expect(result_2.dig("data", "ratingManga", "rating", "mid")).to eq(manga.mid)
      expect(result_2.dig("data", "ratingManga", "rating", "rate")).to eq("bad")
      expect(result_2.dig("data", "ratingManga", "rating", "id")).to eq(
        result.dig("data", "ratingManga", "rating", "id")
      )
    end
  end
end
