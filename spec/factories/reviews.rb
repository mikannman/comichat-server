FactoryBot.define do
  factory :review do
    association :manga, factory: :manga
    association :user, factory: :user
    star { 1 }
    comment { Faker::Lorem.word }
  end
end
