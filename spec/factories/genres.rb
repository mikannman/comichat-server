FactoryBot.define do
  factory :genre do
    genre { Faker::Lorem.word }
    sequence(:gid) { |n| "gid_#{n}" }
  end
end
