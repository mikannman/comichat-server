FactoryBot.define do
  factory :answered_question, class: "Question" do
    association :ask_user, factory: :user
    trait :with_asked_user do
      association :asked_user, factory: :user
    end
    after(:create) do |question|
      FactoryBot.create(:question_genre, question_id: question.id)
    end

    reading_manga_mid { "MyString" }
    readed_manga_mid { "MyString" }
    content { "MyString" }
    all { true }
    after(:create) do |question|
      FactoryBot.create(:answer, question_id: question.id)
    end
  end
end
