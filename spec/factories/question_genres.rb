FactoryBot.define do
  factory :question_genre do
    association :question, factory: :question
    association :genre, factory: :genre
  end
end
