FactoryBot.define do
  factory :like_review do
    association :user, factory: :user
    association :review, factory: :review
  end
end
