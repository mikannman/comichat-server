FactoryBot.define do
  factory :manga do
    sequence(:mid) { |n| "mid_#{n}" }
    title { "MyString" }
    date_published { "2020-11-11" }
    publisher { "MyString" }
    trait :with_two_reviews do
      after(:create) do |manga|
        FactoryBot.create(:review, mid: manga.mid, star: 2)
        FactoryBot.create(:review, mid: manga.mid, star: 1)
      end
    end
    trait :with_3_goot_ratings do
      3.times do
        after(:create) do |manga|
          FactoryBot.create(:rating, mid: manga.mid, rate: "good")
        end
      end
    end
    trait :with_3_normal_ratings do
      3.times do
        after(:create) do |manga|
          FactoryBot.create(:rating, mid: manga.mid, rate: "normal")
        end
      end
    end
    trait :with_3_bad_ratings do
      3.times do
        after(:create) do |manga|
          FactoryBot.create(:rating, mid: manga.mid, rate: "bad")
        end
      end
    end
  end
end
