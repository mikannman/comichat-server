FactoryBot.define do
  factory :like_reply do
    association :user, factory: :user
    association :reply, factory: :reply
  end
end
