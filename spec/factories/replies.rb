FactoryBot.define do
  factory :reply do
    association :answer, factory: :answer
    association :user, factory: :user
    content { Faker::Lorem.word }
  end
end
