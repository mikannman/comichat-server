FactoryBot.define do
  factory :kana do
    sequence(:mid) { |n| "mid_#{n}" }
    kana { "MyString" }
  end
end
