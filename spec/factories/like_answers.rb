FactoryBot.define do
  factory :like_answer do
    association :user, factory: :user
    association :answer, factory: :answer
  end
end
