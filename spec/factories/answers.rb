FactoryBot.define do
  factory :answer do
    association :question, factory: :question
    association :user, factory: :user
    content { Faker::Lorem.word }
    trait :with_10_replies do
      10.times do
        after(:create) do |answer|
          FactoryBot.create(:reply, answer_id: answer.id)
        end
      end
    end
    trait :with_10_likes do
      10.times do
        after(:create) do |answer|
          FactoryBot.create(:like_answer, answer_id: answer.id)
        end
      end
    end
  end
end
