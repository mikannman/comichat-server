FactoryBot.define do
  factory :rating do
    association :manga, factory: :manga
    association :user, factory: :user
    rate { 0 }
  end
end
