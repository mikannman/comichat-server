FactoryBot.define do
  factory :question do
    association :ask_user, factory: :user
    trait :with_asked_user do
      association :asked_user, factory: :user
    end
    reading_manga_mid { "MyString" }
    readed_manga_mid { "MyString" }
    content { Faker::Lorem.word }
    all { true }
    after(:create) do |question|
      FactoryBot.create(:question_genre, question_id: question.id)
    end

    trait :with_10_answers do
      10.times do
        after(:create) do |question|
          FactoryBot.create(:answer, question_id: question.id)
        end
      end
    end
  end
end
