FactoryBot.define do
  factory :user do
    name { Faker::Name.name }
    sequence(:user_id) { |n| "id_#{n}" }
    sequence(:email) { |n| "mail_#{n}@example.com" }
    sequence(:uid) { |n| "uid_#{n}" }
    imageURL { "MyString" }
    provider { "MyString" }
    message { "test message" }
    trait :with_3_reviews do
      after(:create) do |user|
        FactoryBot.create_list(:review, 3, user_id: user.id)
      end
    end
    trait :with_2_reviews do
      after(:create) do |user|
        FactoryBot.create_list(:review, 2, user_id: user.id)
      end
    end
  end
end
