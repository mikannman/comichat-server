class CreateKanas < ActiveRecord::Migration[6.0]
  def change
    create_table :kanas do |t|
      t.string :kana
      t.string :mid, null: false

      t.timestamps
    end

    add_index :kanas, :kana, unique: true
  end
end
