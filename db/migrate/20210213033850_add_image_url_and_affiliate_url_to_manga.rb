class AddImageUrlAndAffiliateUrlToManga < ActiveRecord::Migration[6.0]
  def change
    add_column :mangas, :image_url, :string
    add_column :mangas, :affiliate_url, :string
  end
end
