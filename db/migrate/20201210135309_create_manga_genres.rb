class CreateMangaGenres < ActiveRecord::Migration[6.0]
  def change
    create_table :manga_genres do |t|
      t.string :mid
      t.string :gid

      t.timestamps
    end
    add_index :manga_genres, :mid
    add_index :manga_genres, :gid
    add_index :manga_genres, [:mid, :gid], unique: true
  end
end
