class CreateWannaReads < ActiveRecord::Migration[6.0]
  def change
    create_table :wanna_reads do |t|
      t.references :user, null: false, foreign_key: true
      t.string :mid

      t.timestamps
    end
  end
end
