class CreateMangaLabels < ActiveRecord::Migration[6.0]
  def change
    create_table :manga_labels do |t|
      t.string :mid
      t.string :lid

      t.timestamps
    end
    add_index :manga_labels, :mid
    add_index :manga_labels, :lid
    add_index :manga_labels, [:mid, :lid], unique: true
  end
end
