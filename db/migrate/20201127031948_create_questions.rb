class CreateQuestions < ActiveRecord::Migration[6.0]
  def change
    create_table :questions do |t|
      t.integer :ask_user_id
      t.integer :asked_user_id, default: 0
      t.string :genre
      t.string :reading_manga_mid
      t.string :readed_manga_mid
      t.string :content
      t.boolean :all

      t.timestamps
    end

    add_index :questions, :ask_user_id
    add_index :questions, :asked_user_id
  end
end
