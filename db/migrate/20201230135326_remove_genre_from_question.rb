class RemoveGenreFromQuestion < ActiveRecord::Migration[6.0]
  def change
    remove_column :questions, :genre
  end
end
