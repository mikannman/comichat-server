class CreateMangas < ActiveRecord::Migration[6.0]
  def change
    create_table :mangas, id: false do |t|
      t.string :title
      t.date :date_published
      t.string :publisher
      t.string :synopsis
      t.string :mid, null: false, primary_key: true

      t.timestamps
    end
  end
end
