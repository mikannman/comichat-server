class CreateReviews < ActiveRecord::Migration[6.0]
  def change
    create_table :reviews do |t|
      t.string :mid
      t.references :user, null: false, foreign_key: true
      t.integer :star
      t.string :comment
      t.boolean :netabare, default: false

      t.timestamps
    end

    add_index :reviews, :mid
  end
end
