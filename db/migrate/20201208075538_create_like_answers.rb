class CreateLikeAnswers < ActiveRecord::Migration[6.0]
  def change
    create_table :like_answers do |t|
      t.references :user, null: false, foreign_key: true
      t.references :answer, null: false, foreign_key: true

      t.timestamps
    end
  end
end
