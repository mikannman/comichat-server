class CreateLabels < ActiveRecord::Migration[6.0]
  def change
    create_table :labels, id: false do |t|
      t.string :label
      t.string :lid, null: false, primary_key: true

      t.timestamps
    end
  end
end
