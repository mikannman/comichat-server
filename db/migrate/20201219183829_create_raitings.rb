class CreateRaitings < ActiveRecord::Migration[6.0]
  def change
    create_table :ratings do |t|
      t.references :user, null: false, foreign_key: true
      t.string :mid
      t.integer :rate, default: 0

      t.timestamps
    end
    add_index :ratings, :mid
  end
end
