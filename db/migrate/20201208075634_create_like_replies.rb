class CreateLikeReplies < ActiveRecord::Migration[6.0]
  def change
    create_table :like_replies do |t|
      t.references :user, null: false, foreign_key: true
      t.references :reply, null: false, foreign_key: true

      t.timestamps
    end
  end
end
