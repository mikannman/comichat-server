class CreateUsers < ActiveRecord::Migration[6.0]
  def change
    create_table :users do |t|
      t.string :name, null: false
      t.string :user_id, null: false
      t.string :email, null: false
      t.string :message
      t.string :uid, null: false
      t.string :imageURL
      t.string :provider

      t.timestamps
    end

    add_index :users, :name
    add_index :users, :uid, unique: true
    add_index :users, :email, unique: true
    add_index :users, :user_id, unique: true
  end
end
