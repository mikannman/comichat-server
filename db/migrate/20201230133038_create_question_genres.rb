class CreateQuestionGenres < ActiveRecord::Migration[6.0]
  def change
    create_table :question_genres do |t|
      t.string :gid, null: false
      t.references :question, null: false, foreign_key: true

      t.timestamps
    end

    add_index :question_genres, :gid
  end
end
