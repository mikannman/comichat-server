class CreateGenres < ActiveRecord::Migration[6.0]
  def change
    create_table :genres, id: false do |t|
      t.string :genre
      t.string :gid, null: false, primary_key: true

      t.timestamps
    end
  end
end
