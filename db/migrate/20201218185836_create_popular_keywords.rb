class CreatePopularKeywords < ActiveRecord::Migration[6.0]
  def change
    create_table :popular_keywords do |t|
      t.string :word

      t.timestamps
    end

    add_index :popular_keywords, :word
  end
end
