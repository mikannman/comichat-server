class CreateMangaCreators < ActiveRecord::Migration[6.0]
  def change
    create_table :manga_creators do |t|
      t.string :mid
      t.string :cid

      t.timestamps
    end
    add_index :manga_creators, :mid
    add_index :manga_creators, :cid
    add_index :manga_creators, [:mid, :cid], unique: true
  end
end
