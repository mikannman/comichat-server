class CreateCreators < ActiveRecord::Migration[6.0]
  def change
    create_table :creators, id: false do |t|
      t.string :creator
      t.string :yomi
      t.string :cid, null: false, primary_key: true

      t.timestamps
    end
  end
end
