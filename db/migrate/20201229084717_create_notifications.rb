class CreateNotifications < ActiveRecord::Migration[6.0]
  def change
    create_table :notifications do |t|
      t.integer :visitor_id, null: false
      t.integer :visited_id, null: false
      t.integer :answer_id
      t.integer :reply_id
      t.integer :review_id
      t.integer :action, default: 0, null: false
      t.boolean :checked, default: false, null: false

      t.timestamps
    end

    add_index :notifications, :visitor_id
    add_index :notifications, :visited_id
    add_index :notifications, :answer_id
    add_index :notifications, :reply_id
    add_index :notifications, :review_id
  end
end
