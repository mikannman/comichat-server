# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `rails
# db:schema:load`. When creating a new database, `rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2021_02_13_033850) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "answers", force: :cascade do |t|
    t.bigint "question_id", null: false
    t.bigint "user_id", null: false
    t.string "content"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["question_id"], name: "index_answers_on_question_id"
    t.index ["user_id"], name: "index_answers_on_user_id"
  end

  create_table "creators", primary_key: "cid", id: :string, force: :cascade do |t|
    t.string "creator"
    t.string "yomi"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "genres", primary_key: "gid", id: :string, force: :cascade do |t|
    t.string "genre"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "kanas", force: :cascade do |t|
    t.string "kana"
    t.string "mid", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["kana"], name: "index_kanas_on_kana", unique: true
  end

  create_table "labels", primary_key: "lid", id: :string, force: :cascade do |t|
    t.string "label"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "like_answers", force: :cascade do |t|
    t.bigint "user_id", null: false
    t.bigint "answer_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["answer_id"], name: "index_like_answers_on_answer_id"
    t.index ["user_id"], name: "index_like_answers_on_user_id"
  end

  create_table "like_replies", force: :cascade do |t|
    t.bigint "user_id", null: false
    t.bigint "reply_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["reply_id"], name: "index_like_replies_on_reply_id"
    t.index ["user_id"], name: "index_like_replies_on_user_id"
  end

  create_table "like_reviews", force: :cascade do |t|
    t.bigint "user_id", null: false
    t.bigint "review_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["review_id"], name: "index_like_reviews_on_review_id"
    t.index ["user_id"], name: "index_like_reviews_on_user_id"
  end

  create_table "manga_creators", force: :cascade do |t|
    t.string "mid"
    t.string "cid"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["cid"], name: "index_manga_creators_on_cid"
    t.index ["mid", "cid"], name: "index_manga_creators_on_mid_and_cid", unique: true
    t.index ["mid"], name: "index_manga_creators_on_mid"
  end

  create_table "manga_genres", force: :cascade do |t|
    t.string "mid"
    t.string "gid"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["gid"], name: "index_manga_genres_on_gid"
    t.index ["mid", "gid"], name: "index_manga_genres_on_mid_and_gid", unique: true
    t.index ["mid"], name: "index_manga_genres_on_mid"
  end

  create_table "manga_labels", force: :cascade do |t|
    t.string "mid"
    t.string "lid"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["lid"], name: "index_manga_labels_on_lid"
    t.index ["mid", "lid"], name: "index_manga_labels_on_mid_and_lid", unique: true
    t.index ["mid"], name: "index_manga_labels_on_mid"
  end

  create_table "mangas", primary_key: "mid", id: :string, force: :cascade do |t|
    t.string "title"
    t.date "date_published"
    t.string "publisher"
    t.string "synopsis"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.string "image_url"
    t.string "affiliate_url"
  end

  create_table "notifications", force: :cascade do |t|
    t.integer "visitor_id", null: false
    t.integer "visited_id", null: false
    t.integer "answer_id"
    t.integer "reply_id"
    t.integer "review_id"
    t.integer "action", default: 0, null: false
    t.boolean "checked", default: false, null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.integer "question_id"
    t.index ["answer_id"], name: "index_notifications_on_answer_id"
    t.index ["question_id"], name: "index_notifications_on_question_id"
    t.index ["reply_id"], name: "index_notifications_on_reply_id"
    t.index ["review_id"], name: "index_notifications_on_review_id"
    t.index ["visited_id"], name: "index_notifications_on_visited_id"
    t.index ["visitor_id"], name: "index_notifications_on_visitor_id"
  end

  create_table "popular_keywords", force: :cascade do |t|
    t.string "word"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["word"], name: "index_popular_keywords_on_word"
  end

  create_table "question_genres", force: :cascade do |t|
    t.string "gid", null: false
    t.bigint "question_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["gid"], name: "index_question_genres_on_gid"
    t.index ["question_id"], name: "index_question_genres_on_question_id"
  end

  create_table "questions", force: :cascade do |t|
    t.integer "ask_user_id"
    t.integer "asked_user_id", default: 0
    t.string "reading_manga_mid"
    t.string "readed_manga_mid"
    t.string "content"
    t.boolean "all"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["ask_user_id"], name: "index_questions_on_ask_user_id"
    t.index ["asked_user_id"], name: "index_questions_on_asked_user_id"
  end

  create_table "ratings", force: :cascade do |t|
    t.bigint "user_id", null: false
    t.string "mid"
    t.integer "rate", default: 0
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["mid"], name: "index_ratings_on_mid"
    t.index ["user_id"], name: "index_ratings_on_user_id"
  end

  create_table "readeds", force: :cascade do |t|
    t.bigint "user_id", null: false
    t.string "mid"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["user_id"], name: "index_readeds_on_user_id"
  end

  create_table "relationships", force: :cascade do |t|
    t.integer "follower_id"
    t.integer "followed_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["followed_id", "follower_id"], name: "index_relationships_on_followed_id_and_follower_id", unique: true
    t.index ["followed_id"], name: "index_relationships_on_followed_id"
    t.index ["follower_id"], name: "index_relationships_on_follower_id"
  end

  create_table "replies", force: :cascade do |t|
    t.bigint "answer_id", null: false
    t.bigint "user_id", null: false
    t.string "content"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["answer_id"], name: "index_replies_on_answer_id"
    t.index ["user_id"], name: "index_replies_on_user_id"
  end

  create_table "reviews", force: :cascade do |t|
    t.string "mid"
    t.bigint "user_id", null: false
    t.integer "star"
    t.string "comment"
    t.boolean "netabare", default: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["mid"], name: "index_reviews_on_mid"
    t.index ["user_id"], name: "index_reviews_on_user_id"
  end

  create_table "users", force: :cascade do |t|
    t.string "name", null: false
    t.string "user_id", null: false
    t.string "email", null: false
    t.string "message"
    t.string "uid", null: false
    t.string "imageURL"
    t.string "provider"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.boolean "guest"
    t.string "hira"
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["name"], name: "index_users_on_name"
    t.index ["uid"], name: "index_users_on_uid", unique: true
    t.index ["user_id"], name: "index_users_on_user_id", unique: true
  end

  create_table "wanna_reads", force: :cascade do |t|
    t.bigint "user_id", null: false
    t.string "mid"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["user_id"], name: "index_wanna_reads_on_user_id"
  end

  add_foreign_key "answers", "questions"
  add_foreign_key "answers", "users"
  add_foreign_key "like_answers", "answers"
  add_foreign_key "like_answers", "users"
  add_foreign_key "like_replies", "replies"
  add_foreign_key "like_replies", "users"
  add_foreign_key "like_reviews", "reviews"
  add_foreign_key "like_reviews", "users"
  add_foreign_key "question_genres", "questions"
  add_foreign_key "ratings", "users"
  add_foreign_key "readeds", "users"
  add_foreign_key "replies", "answers"
  add_foreign_key "replies", "users"
  add_foreign_key "reviews", "users"
  add_foreign_key "wanna_reads", "users"
end
