sample_sentences = [
  'サンプル',
  'サンプル',
  'サンプル',
  'サンプル',
  'サンプル',
]

def sample_paragraph(reviews)
  array = (1..5).to_a
  sentences = reviews.sample(array.sample).join("\n")
end

user = User.find_by(name: "サンプル")

users = User.limit(5)

10.times do |i|
  Question.create(
    ask_user_id: user.id,
    asked_user_id: [nil, *users.map(&:id)].sample,
    content: sample_paragraph(sample_sentences),
    all: [true, false].sample
  )
end

questions = Question.limit(5)

questions.each do |question|
  3.times do
    users.each do |u|
      question.answers.create(
        user_id: u.id,
        content: sample_paragraph(sample_sentences),
      )
    end
  end
end

users.each do |u|
  q = questions.first
  answer = q.answers.create(
    user_id: u.id,
    content: sample_paragraph(sample_sentences),
  )
  answer.create_answer_notification(u, user.id)
end

other_question = Question.create(
  ask_user_id: users[4].id,
  asked_user_id: [nil, *users.map(&:id)].sample,
  content: sample_paragraph(sample_sentences),
  all: [true, false].sample
)

my_answer = other_question.answers.create(
  user_id: user.id,
  content: sample_paragraph(sample_sentences)
)

other_reply = my_answer.replies.create(
  user_id: users[4].id,
  content: sample_paragraph(sample_sentences)
)

other_reply.create_reply_notification(users[4], user.id)

answers = Answer.all

answers[-5..-1].each do |answer|
  3.times do
    users.each do |u|
      answer.replies.create(
        user_id: u.id,
        content: sample_paragraph(sample_sentences),
      )
    end
  end
end
