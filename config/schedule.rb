rails_env = :development
set :environment, rails_env
ENV.each { |k, v| env(k, v) }
set :output, error: "/server/log/crontab_error.log", standard: "/server/log/crontab.log"

every "30 10 12 12 *" do
  rake "register_dbs:manga"
end

# every 10.minutes do
#   rake 'test'
# end

# every 3.minutes do
#   rake 'test:sample'
# end
