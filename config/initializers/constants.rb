class Const
  SECRET_KEY = Rails.application.credentials.secret_key_base
  # 定数の再代入を防ぐためクラス自体をfreezeする
  freeze
end