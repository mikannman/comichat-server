# -*- encoding: utf-8 -*-
# stub: romkan 0.4.0 ruby lib

Gem::Specification.new do |s|
  s.name = "romkan".freeze
  s.version = "0.4.0"

  s.required_rubygems_version = Gem::Requirement.new(">= 0".freeze) if s.respond_to? :required_rubygems_version=
  s.require_paths = ["lib".freeze]
  s.authors = ["Satoru Takabayashi".freeze]
  s.date = "2011-03-17"
  s.description = "Ruby/Romkan is a Romaji <-> Kana conversion library for\nRuby. It can convert a Japanese Romaji string to a Japanese\nKana string or vice versa.\n\nTha latest version of Ruby/Romkan is available at\n((<URL:http://namazu.org/~satoru/ruby-romkan/>))\n".freeze
  s.homepage = "http://0xcc.net/ruby-romkan/".freeze
  s.licenses = ["Ruby's".freeze]
  s.rubygems_version = "3.1.4".freeze
  s.summary = "a Romaji <-> Kana conversion library for Ruby.".freeze

  s.installed_by_version = "3.1.4" if s.respond_to? :installed_by_version
end
