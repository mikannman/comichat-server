require './lib/tasks/utils/update_manga_data.rb'

desc "漫画データベースにデータを追加"
task update_manga_data: :environment do
  update_manga_data
  puts '全部終了'
end
