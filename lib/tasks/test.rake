require "erb"
include ERB::Util

desc "クローラーのテスト"
task test: :environment do
  logger = Logger.new(STDERR)

  # Selenium::WebDriver::Chrome.path = ENV.fetch("GOOGLE_CHROME_BIN", nil)

  options = Selenium::WebDriver::Chrome::Options.new

  # options.binary = ENV.fetch("GOOGLE_CHROME_SHIM", nil)

  options.add_argument("--headless")
  options.add_argument("--disable-gpu")
  options.add_argument("--no-sandbox")
  options.add_argument("--window-size=1400,1400")
  options.add_argument("--disable-dev-shm-usage")

  driver = Selenium::WebDriver.for :chrome, options: options

  wait = Selenium::WebDriver::Wait.new(timeout: 10)

  driver.get("https://comicspace.jp/list/genre/8")

  wait.until { driver.find_element(:xpath, '//*[@id="tvi-66091"]/div[1]/div[1]/h3/a').displayed? }

  title = driver.find_element(:xpath, '//*[@id="tvi-66091"]/div[1]/div[1]/h3/a')

  puts title.text
end
