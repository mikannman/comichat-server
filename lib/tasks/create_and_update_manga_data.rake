require './lib/tasks/utils/create_manga_data.rb'
require './lib/tasks/utils/update_manga_data.rb'

desc "漫画データベース作成し、データの追加"
task create_and_update_manga_data: :environment do
  create_manga_data
  update_manga_data
  puts '全部終了'
end
