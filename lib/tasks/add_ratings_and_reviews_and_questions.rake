task :add_ratings_and_reviews_and_questions => :environment do
  genres = Genre.all
  user = User.find_by(name: "サンプル")

  raise RuntimeError, "サンプルアカウントを作成してください" unless user

  5.times do |idx|
    User.create(
      name: "sample_#{idx}",
      user_id: "user_id_#{idx}",
      email: "sample_#{idx}@example.com",
      uid: "uid_#{idx}",
      hira: "sample_#{idx}".to_hira
    )
  end

  sample_reviews = [
    'これは読むべき！！',
    'おもしろいです！',
    'おすすめ！？',
    '読んでない人は人生半分損してる',
    'ハンカチ必須！！！'
  ]

  users = User.limit(5)

  def sample_paragraph(reviews)
    array = (1..5).to_a
    sentences = reviews.sample(array.sample).join("\n")
  end

  genres.each do |genre|
    genre.mangas.limit(5).each do |manga|
      users.each do |u|
        manga.reviews.create(
          user_id: u.id,
          comment: sample_paragraph(sample_reviews),
          star: (1..5).to_a.sample,
          netabare: [true, false].sample
        )
        u.rating_manga(manga.mid, ["good", "normal", "bad"].sample)
      end
    end
  end

  sample_sentences = [
    'サンプル',
    'サンプル',
    'サンプル',
    'サンプル',
    'サンプル',
  ]

  other_users = User.where.not(name: 'サンプル').limit(5)

  10.times do |i|
    Question.create(
      ask_user_id: user.id,
      asked_user_id: [nil, *other_users.map(&:id)].sample,
      content: sample_paragraph(sample_sentences),
      all: [true, false].sample
    )
  end

  questions = Question.limit(5)

  questions.each do |question|
    3.times do
      other_users.each do |u|
        question.answers.create(
          user_id: u.id,
          content: sample_paragraph(sample_sentences),
        )
      end
    end
  end

  other_users.each do |u|
    q = questions.first
    answer = q.answers.create(
      user_id: u.id,
      content: sample_paragraph(sample_sentences),
    )
    answer.create_answer_notification(u, user.id)
  end

  other_question = Question.create(
    ask_user_id: other_users[4].id,
    asked_user_id: [nil, other_users[3].id].sample,
    content: sample_paragraph(sample_sentences),
    all: [true, false].sample
  )

  my_answer = other_question.answers.create(
    user_id: user.id,
    content: sample_paragraph(sample_sentences)
  )

  other_reply = my_answer.replies.create(
    user_id: other_users[4].id,
    content: sample_paragraph(sample_sentences)
  )

  other_reply.create_reply_notification(other_users[4], user.id)

  answers = Answer.all

  answers[-5..-1].each do |answer|
    3.times do
      other_users.each do |u|
        answer.replies.create(
          user_id: u.id,
          content: sample_paragraph(sample_sentences),
        )
      end
    end
  end
end
