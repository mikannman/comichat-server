require './lib/tasks/utils/create_manga_data.rb'

desc "漫画データベースの作成"
task create_manga_data: :environment do
  create_manga_data
  puts '全部終了'
end
