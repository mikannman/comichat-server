require 'json'
require 'net/https'
require 'uri'
require 'logger'
require "erb"
include ERB::Util

def create_manga_data
  now = Time.zone.now

  mangas         = []
  kanas          = []
  creators       = []
  manga_creators = []
  labels         = []
  manga_labels   = []
  genres         = []
  manga_genres   = []

  searched_titles = []

  options = Selenium::WebDriver::Chrome::Options.new

  options.add_argument("--headless")
  options.add_argument("--disable-gpu")
  options.add_argument("--no-sandbox")
  options.add_argument("--window-size=1400,1400")
  options.add_argument("--disable-dev-shm-usage")

  driver = Selenium::WebDriver.for :chrome, options: options

  wait = Selenium::WebDriver::Wait.new(timeout: 7)

  puts '作成を開始します'

  ('あ'..'ん').each do |word|
    puts "#{word}を検索中です"
    register_manga(
      word,
      mangas,
      kanas,
      creators,
      manga_creators,
      labels,
      manga_labels,
      genres,
      manga_genres,
      driver,
      wait,
      searched_titles,
      now
    )
  rescue
    next
  end

  ('ア'..'ン').each do |word|
    puts "#{word}を検索中です"
    register_manga(
      word,
      mangas,
      kanas,
      creators,
      manga_creators,
      labels,
      manga_labels,
      genres,
      manga_genres,
      driver,
      wait,
      searched_titles,
      now
    )
  rescue
    next
  end

  ('a'..'z').each do |word|
    puts "#{word}を検索中です"
    register_manga(
      word,
      mangas,
      kanas,
      creators,
      manga_creators,
      labels,
      manga_labels,
      genres,
      manga_genres,
      driver,
      wait,
      searched_titles,
      now
    )
  rescue
    next
  end

  puts mangas.uniq.length
  puts kanas.uniq.length
  puts creators.uniq.length
  puts manga_creators.uniq.length
  puts labels.uniq.length
  puts manga_labels.uniq.length
  puts genres.uniq.length
  puts manga_genres.uniq.length

  Manga.insert_all mangas.uniq
  Kana.insert_all kanas.uniq
  Creator.insert_all creators.uniq
  MangaCreator.insert_all manga_creators.uniq
  Label.insert_all labels.uniq
  MangaLabel.insert_all manga_labels.uniq
  Genre.insert_all genres.uniq
  MangaGenre.insert_all manga_genres.uniq

  puts '終了しました'
end

def fetch_manga_data(word, offset, limit)
  query = URI.encode_www_form(
    {
      fieldId: 'manga',
      categoryId: 'cm-item',
      subcategoryId: 'cm101',
      q: word,
      sort: 'date',
      offset: offset,
      limit: limit,
    }
  )

  uri = URI.parse("https://mediaarts-db.bunka.go.jp/api/search?#{query}")

  http = Net::HTTP.new(uri.host, uri.port)
  http.use_ssl = true

  req = Net::HTTP::Get.new(uri.request_uri)
  res = http.request(req)

  res_data = JSON.parse(res.body)
end

def register_manga(
  keyword,
  mangas,
  kanas,
  creators,
  manga_creators,
  labels,
  manga_labels,
  genres,
  manga_genres,
  driver,
  wait,
  searched_titles,
  now
)
  logger = Logger.new(STDERR)

  data = fetch_manga_data(keyword, 0, 100)

  data['record'].each do |record|
    init_mangas         = []
    init_kanas          = []
    init_creators       = []
    init_manga_creators = []
    init_labels         = []
    init_manga_labels   = []
    init_genres         = []
    init_manga_genres   = []

    next if !record['metadata']['schema:creator'].first || !record['metadata']['schema:name'].first

    mid = record['metadata']['schema:name'].first.to_mid
    title = record['metadata']['schema:name'].first.to_mid
    publisher = record['metadata']['schema:publisher'].first.gsub(/[[:space:]]+∥[[:space:]]+.+/, '')
    date_published = DateTime.parse(record['metadata']['schema:datePublished']) rescue nil

    next if searched_titles.include?(title)

    puts "#{mid}を検索中です"

    searched_titles << title

    driver.get("https://comicspace.jp/search/title/#{url_encode(title.gsub(/[[:space:]]:[[:space:]](.+)/, ""))}")

    wait.until { driver.find_element(:css, ".mainList > article:nth-child(1) > .main > .left > h3 > a").displayed? }

    scraping_titles = driver.find_elements(:css, ".mainList > article > .main > .left > h3 > a")

    scraping_title = nil

    scraping_titles.each do |st|
      if title == st.text \
        || title == st.text.gsub(/[[:space:]].+/, '') \
        || title.gsub(/[[:space:]]:[[:space:]](.+)/, " -\\1-") == st.text \
        || title.gsub(/[[:space:]]:[[:space:]](.+)/, " \\1") == st.text \
        || title.gsub(/[[:space:]]:[[:space:]](.+)/, "") == st.text \
        || st.text.include?(title) \
        || st.text.include?(title.gsub(/[[:space:]]:[[:space:]](.+)/, " -\\1-")) \
        || st.text.include?(title.gsub(/[[:space:]]:[[:space:]](.+)/, " \\1"))

        scraping_title = st
        break
      else
        next
      end
    end

    raise "タイトルが一致しません" unless scraping_title

    scraping_title.click

    wait.until { driver.find_element(:xpath, '//*[@id="titleSingleUpper"]/h2').displayed? }

    t = driver.find_element(:xpath, '//*[@id="titleSingleUpper"]/h2')
    scraping_genres = driver.find_elements(:xpath, '//dt[contains(text(), "ジャンル")]/following-sibling::dd[1]/a') rescue nil

    scraping_genres.each do |genre|
      init_genres << {
        gid: genre.text,
        genre: genre.text,
        created_at: now,
        updated_at: now
      }

      init_manga_genres << {
        mid: mid,
        gid: genre.text,
        created_at: now,
        updated_at: now
      }
    end

    init_mangas << {
      mid: mid,
      title: title,
      publisher: publisher,
      date_published: date_published,
      created_at: now,
      updated_at: now,
    }

    kanas_array = []

    if record['metadata']['schema:name xml:lang="ja-Hrkt"']
      record['metadata']['schema:name xml:lang="ja-Hrkt"'].each do |k|
        kanas_array << k.gsub(/[[:space:]]/, '').to_hira
      end
    end
    new_kanas_array = [*kanas_array, *title.to_kanas, title]

    new_kanas_array.each do |k|
      init_kanas << {
        mid: mid,
        kana: k,
        created_at: now,
        updated_at: now,
      }
    end

    record['metadata']['schema:creator'].each.with_index do |c, idx|
      cid = c.sub(/^\[.*\]/, '')
      creator = c.sub(/^\[.*\]/, '')
      begin
        yomi = creator.gsub(/[一-龠々]+/) { |n| n.to_kanhira }.to_hira
        if record['metadata']['ma:creatorLabel']
          yomi = record['metadata']['ma:creatorLabel'][idx].to_creator_yomi.to_hira
        end
      rescue
        yomi = creator.to_hira
      end

      init_creators << {
        cid: cid,
        creator: creator,
        yomi: yomi,
        created_at: now,
        updated_at: now,
      }

      init_manga_creators << {
        mid: mid,
        cid: cid,
        created_at: now,
        updated_at: now,
      }
    end

    if record['metadata']['schema:brand']
      record['metadata']['schema:brand'].each do |lid|
        init_labels << {
          lid: lid,
          label: lid,
          created_at: now,
          updated_at: now
        }

        init_manga_labels << {
          mid: mid,
          lid: lid,
          created_at: now,
          updated_at: now
        }
      end
    end

    mangas.concat         init_mangas
    kanas.concat          init_kanas
    creators.concat       init_creators
    manga_creators.concat init_manga_creators
    labels.concat         init_labels
    manga_labels.concat   init_manga_labels
    genres.concat         init_genres
    manga_genres.concat   init_manga_genres
  rescue => e
    error_message = <<~TEXT
        #{e.message}
        #{e.backtrace}
        問題がおこったのは以下です
        #{record['metadata']['schema:name'].first if record['metadata']['schema:name']}
      TEXT
    logger << error_message

    next
  end
end

class String
  def to_mid
    self.gsub(/[[:space:]]=[[:space:]].+/, '')
  end

  def to_dm
    self.gsub(/Dr./, 'ドクター')
      .gsub(/Mr./, 'ミスター')
      .gsub(/Mrs./, 'ミセス')
  end

  def to_kanas
    array = []
    array << self
    array << self.gsub(/[一-龠々]+/) { |n| n.to_kanhira }.to_hira
    array << self.to_dm.to_kana.to_hira
    array << self.to_kana.to_hira
    array
  end

  def to_creator_yomi
    result = []
    self.gsub(/[∥|,][[:space:]]([ァ-ン]+)/) do |n|
      result << $1
    end
    result.join.to_hira
  end
end
