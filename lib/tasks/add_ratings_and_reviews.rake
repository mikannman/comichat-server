task :add_ratings_and_reviews => :environment do
  5.times do |idx|
    User.create(
      name: "sample_#{idx}",
      user_id: "user_#{idx}",
      email: "sample_#{idx}@example.com",
      uid: "uid_#{idx}"
    )
  end
  genres = Genre.all
  users = User.limit(5)
  sample_reviews = [
    'これは読むべき！！',
    'おもしろいです！',
    'おすすめ！？',
    '読んでない人は人生半分損してる',
    'ハンカチ必須！！！'
  ]

  def sample_paragraph(reviews)
    array = (1..5).to_a
    sentences = reviews.sample(array.sample).join("\n")
  end

  genres.each do |genre|
    genre.mangas.limit(5).each do |manga|
      users.each do |u|
        manga.reviews.create(
          user_id: u.id,
          comment: sample_paragraph(sample_reviews),
          star: (1..5).to_a.sample,
          netabare: [true, false].sample
        )
        u.rating_manga(manga.mid, ["good", "normal", "bad"].sample)
      end
    end
  end
end
