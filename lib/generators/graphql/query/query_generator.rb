module Graphql
  module Generators
    class QueryGenerator < Rails::Generators::NamedBase
      source_root File.expand_path('templates', __dir__)
      desc "Create a Relay Classic query by name"

      argument :name, type: :string

      def create_query_file
        create_file "app/graphql/queries/#{name}.rb", <<~FILE
        module Queries
          class #{class_name} < BaseQuery
            type Types::なんとかType, null: false
            description "なんとか"

            # argument :name, String, required: false, default_value: nil

            def resolve()
              # なんかの処理
            rescue => e
              GraphQL::ExecutionError.new(e.message)
            end
          end
        end
        FILE

        sentinel = /class .*QueryType\s*<\s*[^\s]+?\n/m
        inject_into_file "app/graphql/types/query_type.rb",
                         "    field :#{file_name}, resolver: Queries::#{class_name}\n",
                         after: sentinel, verbose: false, force: false
      end
    end
  end
end
