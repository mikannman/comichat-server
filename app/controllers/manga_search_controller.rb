require "json"
require "net/https"
require "uri"

class MangaSearchController < ApplicationController
  def search
    word = params[:word]
    query = URI.encode_www_form(
      {
        fieldId: "manga",
        categoryId: "cm-item",
        subcategoryId: "cm101",
        keyword: word,
        sort: "date",
        offset: "1000",
        limit: "1000",
      }
    )

    uri = URI.parse("https://mediaarts-db.bunka.go.jp/api/search?#{query}")

    http = Net::HTTP.new(uri.host, uri.port)
    http.use_ssl = true

    req = Net::HTTP::Get.new(uri.request_uri)
    res = http.request(req)

    res_data = JSON.parse(res.body)

    render json: res_data
  end
end
