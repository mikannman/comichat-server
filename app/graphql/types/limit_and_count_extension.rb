module Types
  class LimitAndCountExtension < GraphQL::Schema::FieldExtension
    def after_resolve(value:, **rest)
      {
        value: value[0..1],
        count: value.size,
      }
    end
  end
end
