module Types
  class UserType < Types::BaseObject
    field :id, ID, null: false
    field :user_id, String, null: false
    field :name, String, null: true
    field :uid, String, null: true
    field :email, String, null: true
    field :imageURL, String, null: true
    field :provider, String, null: true
    field :message, String, null: true
    field :guest, Boolean, null: true
    field :following, Types::ExtensionCountType, null: true do
      extension(CountExtension)
    end
    def following
      Loaders::AssociationLoader.for(User, :active_relationships).load(object)
    end
    field :followers, Types::ExtensionCountType, null: true do
      extension(CountExtension)
    end
    def followers
      Loaders::AssociationLoader.for(User, :passive_relationships).load(object)
    end
    field :reviews, Types::ExtensionCountType, null: true do
      extension(CountExtension)
    end
    def reviews
      Loaders::AssociationLoader.for(User, :reviews).load(object)
    end
    field :answers, Types::ExtensionCountType, null: true do
      extension(CountExtension)
    end
    def answers
      Loaders::AssociationLoader.for(User, :answers).load(object)
    end
    field :current_user_followed, Types::ExtensionIsExistType, null: true do
      extension(IsExistExtension)
    end
    def current_user_followed
      Loaders::RecordLoader.for(
        Relationship,
        column: :followed_id,
        where: "relationships.follower_id = #{context[:current_user][:user]&.id || 0}",
      ).load(object[:id])
    end
    field :created_at, GraphQL::Types::ISO8601DateTime, null: false
    field :updated_at, GraphQL::Types::ISO8601DateTime, null: false
  end
end
