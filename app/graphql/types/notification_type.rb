module Types
  class NotificationType < Types::BaseObject
    field :id, ID, null: false
    field :visitor_id, Integer, null: false
    field :visited_id, Integer, null: false
    field :answer_id, Integer, null: true
    field :reply_id, Integer, null: true
    field :review_id, Integer, null: true
    field :action, String, null: false
    field :checked, Boolean, null: false
    field :visitor, Types::UserType, null: false
    def visitor
      Loaders::RecordLoader.for(User).load(object[:visitor_id])
    end
    field :visited, Types::UserType, null: false
    def visited
      Loaders::RecordLoader.for(User).load(object[:visited_id])
    end
    field :question, Types::QuestionType, null: true
    def question
      Loaders::RecordLoader.for(Question).load(object[:question_id])
    end
    field :answer, Types::AnswerType, null: true
    def answer
      Loaders::RecordLoader.for(Answer).load(object[:answer_id])
    end
    field :reply, Types::ReplyType, null: true
    def reply
      Loaders::RecordLoader.for(Reply).load(object[:reply_id])
    end
    field :review, Types::ReviewType, null: true
    def review
      Loaders::RecordLoader.for(Review).load(object[:review_id])
    end
    field :created_at, GraphQL::Types::ISO8601DateTime, null: false
    field :updated_at, GraphQL::Types::ISO8601DateTime, null: false
  end
end
