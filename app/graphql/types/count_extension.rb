module Types
  class CountExtension < GraphQL::Schema::FieldExtension
    def after_resolve(value:, **rest)
      {
        count: value.size,
      }
    end
  end
end
