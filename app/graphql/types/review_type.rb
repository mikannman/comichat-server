module Types
  class ReviewType < Types::BaseObject
    field :id, ID, null: false
    field :mid, String, null: false
    field :user_id, Integer, null: false
    field :star, Integer, null: true
    field :comment, String, null: true
    field :netabare, Boolean, null: true
    field :user, Types::UserType, null: false
    def user
      Loaders::RecordLoader.for(User).load(object[:user_id])
    end
    field :manga, Types::MangaType, null: false
    def manga
      Loaders::RecordLoader.for(Manga).load(object[:mid])
    end
    field :like_reviews, Types::ExtensionCountType, null: true do
      extension(CountExtension)
    end
    def like_reviews
      Loaders::AssociationLoader.for(Review, :like_reviews).load(object)
    end
    field :current_user_likes, Types::ExtensionIsExistType, null: true do
      extension(IsExistExtension)
    end
    def current_user_likes
      Loaders::RecordLoader.for(
        LikeReview,
        column: :review_id,
        where: "like_reviews.user_id = #{context[:current_user][:user]&.id || 0}"
      ).load(object[:id])
    end
    field :created_at, GraphQL::Types::ISO8601DateTime, null: false
    field :updated_at, GraphQL::Types::ISO8601DateTime, null: false
  end
end
