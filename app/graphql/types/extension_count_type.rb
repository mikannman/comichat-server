module Types
  class ExtensionCountType < Types::BaseObject
    field :count, Integer, null: false
  end
end
