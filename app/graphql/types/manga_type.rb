module Types
  class MangaType < Types::BaseObject
    field :mid, String, null: false
    field :title, String, null: true
    field :synopsis, String, null: true
    field :date_published, GraphQL::Types::ISO8601Date, null: true
    field :publisher, String, null: true
    field :image_url, String, null: true
    field :affiliate_url, String, null: true
    field :creators, [Types::CreatorType], null: true
    def creators
      Loaders::AssociationLoader.for(Manga, :creators).load(object)
    end
    field :genres, [Types::GenreType], null: true
    def genres
      Loaders::AssociationLoader.for(Manga, :genres).load(object)
    end
    field :labels, [Types::LabelType], null: true
    def labels
      Loaders::AssociationLoader.for(Manga, :labels).load(object)
    end
    field :reviews, Types::ExtensionAverageType, null: true do
      extension(AverageExtension)
    end
    def reviews
      Loaders::AssociationLoader.for(Manga, :reviews).load(object)
    end
    field :current_user_wanna_read, Types::ExtensionIsExistType, null: true do
      extension(IsExistExtension)
    end
    def current_user_wanna_read
      Loaders::RecordLoader.for(
        WannaRead,
        column: :mid,
        where: "wanna_reads.user_id = #{context[:current_user][:user]&.id || 0}",
      ).load(object[:mid])
    end
    field :current_user_readed, Types::ExtensionIsExistType, null: true do
      extension(IsExistExtension)
    end
    def current_user_readed
      Loaders::RecordLoader.for(
        Readed,
        column: :mid,
        where: "readeds.user_id = #{context[:current_user][:user]&.id || 0}",
      ).load(object[:mid])
    end
    field :current_user_rated, Types::RatingType, null: true
    def current_user_rated
      Loaders::RecordLoader.for(
        Rating,
        column: :mid,
        where: "ratings.user_id = #{context[:current_user][:user]&.id || 0}",
      ).load(object[:mid])
    end
    field :created_at, GraphQL::Types::ISO8601DateTime, null: false
    field :updated_at, GraphQL::Types::ISO8601DateTime, null: false
  end
end
