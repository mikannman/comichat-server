module Types
  class LabelType < Types::BaseObject
    field :label, String, null: true
    field :lid, String, null: false
    field :created_at, GraphQL::Types::ISO8601DateTime, null: false
    field :updated_at, GraphQL::Types::ISO8601DateTime, null: false
  end
end
