module Types
  class AverageExtension < GraphQL::Schema::FieldExtension
    def after_resolve(value:, **rest)
      average =
        if value&.loaded?
          stars = value.pluck(:star)
          stars.sum.fdiv(stars.length)
        else
          0
        end
      {
        average: average.round(1) || 0,
      }
    end
  end
end
