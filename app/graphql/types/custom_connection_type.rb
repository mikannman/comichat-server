module Types
  class CustomConnectionType < GraphQL::Types::Relay::BaseConnection
    field :total_count, Integer, null: true
    def total_count
      size = object&.items.size
      if size.is_a?(Hash)
        size.values.inject(:+) || 0
      else
        size || 0
      end
    end

    field :ranking_total_count, Integer, null: true
    def ranking_total_count
      size = object&.items.size
      if size.is_a?(Hash)
        size.length || 0
      else
        size || 0
      end
    end
  end
end
