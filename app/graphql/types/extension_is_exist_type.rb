module Types
  class ExtensionIsExistType < Types::BaseObject
    field :is_exist, Boolean, null: true
  end
end
