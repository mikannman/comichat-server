module Types
  class VerifyUserIdType < Types::BaseObject
    field :type, String, null: false
    field :text, String, null: false
  end
end
