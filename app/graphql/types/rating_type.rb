module Types
  class RatingType < Types::BaseObject
    field :id, ID, null: false
    field :user_id, Integer, null: false
    field :mid, String, null: true
    field :rate, String, null: true
    field :created_at, GraphQL::Types::ISO8601DateTime, null: false
    field :updated_at, GraphQL::Types::ISO8601DateTime, null: false
  end
end
