module Types
  class QuestionType < Types::BaseObject
    field :id, ID, null: false
    field :ask_user_id, Integer, null: true
    field :asked_user_id, Integer, null: true
    field :reading_manga_mid, String, null: true
    field :readed_manga_mid, String, null: true
    field :content, String, null: true
    field :all, Boolean, null: true
    field :ask_user, Types::UserType, null: false
    def ask_user
      Loaders::RecordLoader.for(User).load(object[:ask_user_id])
    end
    field :asked_user, Types::UserType, null: true
    def asked_user
      Loaders::RecordLoader.for(User).load(object[:asked_user_id])
    end
    field :genres, [Types::GenreType], null: true
    def genres
      Loaders::AssociationLoader.for(Question, :genres).load(object)
    end
    field :created_at, GraphQL::Types::ISO8601DateTime, null: false
    field :updated_at, GraphQL::Types::ISO8601DateTime, null: false
  end
end
