module Types
  class CreatorType < Types::BaseObject
    field :creator, String, null: true
    field :yomi, String, null: true
    field :cid, String, null: false
    field :created_at, GraphQL::Types::ISO8601DateTime, null: false
    field :updated_at, GraphQL::Types::ISO8601DateTime, null: false
  end
end
