module Types
  class PopularKeywordType < Types::BaseObject
    field :id, ID, null: false
    field :word, String, null: true
    field :created_at, GraphQL::Types::ISO8601DateTime, null: false
    field :updated_at, GraphQL::Types::ISO8601DateTime, null: false
  end
end
