module Types
  class GenreType < Types::BaseObject
    field :genre, String, null: true
    field :gid, String, null: false
    field :created_at, GraphQL::Types::ISO8601DateTime, null: false
    field :updated_at, GraphQL::Types::ISO8601DateTime, null: false
  end
end
