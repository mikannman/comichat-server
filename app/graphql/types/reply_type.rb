module Types
  class ReplyType < Types::BaseObject
    field :id, ID, null: false
    field :answer_id, Integer, null: false
    field :user_id, Integer, null: false
    field :content, String, null: true
    field :user, Types::UserType, null: false
    def user
      Loaders::RecordLoader.for(User).load(object[:user_id])
    end
    field :answer, Types::AnswerType, null: false
    def answer
      Loaders::RecordLoader.for(Answer).load(object[:answer_id])
    end
    field :like_replies, Types::ExtensionCountType, null: true do
      extension(CountExtension)
    end
    def like_replies
      Loaders::AssociationLoader.for(Reply, :like_replies).load(object)
    end
    field :current_user_likes, Types::ExtensionIsExistType, null: true do
      extension(IsExistExtension)
    end
    def current_user_likes
      Loaders::RecordLoader.for(
        LikeReply,
        column: :reply_id,
        where: "like_replies.user_id = #{context[:current_user][:user]&.id || 0}",
      ).load(object[:id])
    end
    field :created_at, GraphQL::Types::ISO8601DateTime, null: false
    field :updated_at, GraphQL::Types::ISO8601DateTime, null: false
  end
end
