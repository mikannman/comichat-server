module Types
  class AnswerType < Types::BaseObject
    field :id, ID, null: false
    field :question_id, Integer, null: false
    field :user_id, Integer, null: false
    field :content, String, null: true
    field :user, Types::UserType, null: false
    def user
      Loaders::RecordLoader.for(User).load(object[:user_id])
    end
    field :replies, Types::ExtensionCountType, null: true do
      extension(CountExtension)
    end
    def replies
      Loaders::AssociationLoader.for(Answer, :replies).load(object)
    end
    field :like_answers, Types::ExtensionCountType, null: true do
      extension(CountExtension)
    end
    def like_answers
      Loaders::AssociationLoader.for(Answer, :like_answers).load(object)
    end
    field :current_user_likes, Types::ExtensionIsExistType, null: true do
      extension(IsExistExtension)
    end
    def current_user_likes
      Loaders::RecordLoader.for(
        LikeAnswer,
        column: :answer_id,
        where: "like_answers.user_id = #{context[:current_user][:user]&.id || 0}",
      ).load(object[:id])
    end
    field :created_at, GraphQL::Types::ISO8601DateTime, null: false
    field :updated_at, GraphQL::Types::ISO8601DateTime, null: false
  end
end
