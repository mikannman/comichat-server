module Types
  class ExtensionAverageType < Types::BaseObject
    field :average, Float, null: false
  end
end
