module Types
  class IsExistExtension < GraphQL::Schema::FieldExtension
    def after_resolve(value:, **rest)
      {
        is_exist: !value.nil?,
      }
    end
  end
end
