module Types
  class QueryType < Types::BaseObject
    add_field GraphQL::Types::Relay::NodeField
    add_field GraphQL::Types::Relay::NodesField

    field :verify_user_id, resolver: Queries::AuthQuery::VerifyUserId

    field :get_told_questions, resolver: Queries::TeachQuery::GetToldQuestions
    field :get_asked_all_questions, resolver: Queries::TeachQuery::GetAskedAllQuestions
    field :get_asked_me_questions, resolver: Queries::TeachQuery::GetAskedMeQuestions

    field :verify_email, resolver: Queries::VerifyEmail
    field :get_user, resolver: Queries::GetUser
    field :search_user, resolver: Queries::SearchUser
    field :search_manga, resolver: Queries::SearchManga

    field :all_readed_mangas, resolver: Queries::UserQuery::AllReadedMangas
    field :all_wanna_read_mangas, resolver: Queries::UserQuery::AllWannaReadMangas
    field :get_user_reviews, resolver: Queries::UserQuery::GetUserReviews
    field :get_followers, resolver: Queries::UserQuery::GetFollowers
    field :get_following, resolver: Queries::UserQuery::GetFollowing

    field :get_unanswered_list, resolver: Queries::TaughtQuery::GetUnansweredList
    field :get_answered_list, resolver: Queries::TaughtQuery::GetAnsweredList
    field :get_asked_list, resolver: Queries::TaughtQuery::GetAskedList
    field :get_question_answers, resolver: Queries::TaughtQuery::GetQuestionAnswers
    field :get_answer_replies, resolver: Queries::TaughtQuery::GetAnswerReplies

    field :get_manga, resolver: Queries::MangaQuery::GetManga
    field :get_reviews, resolver: Queries::MangaQuery::GetReviews
    field :average_reviews, resolver: Queries::MangaQuery::AverageReviews

    field :get_recommended_users, [Types::UserType], null: false
    def get_recommended_users
      user_id = context[:current_user][:user].id
      recommended_users = User.recommended_users(user_id)
      if recommended_users.size < 5
        add_recommended_users = User.popular_users(
          [*recommended_users&.ids, user_id],
          limit: (5 - recommended_users.size)
        )
        new_list = [*recommended_users, *add_recommended_users]
        if new_list.size < 5
          add_list = User.where.not(id: [*new_list.map(&:id), user_id]).limit(5 - new_list.size)
          return [*new_list, *add_list]
        end

        return new_list
      end
      recommended_users
    end

    field :get_popular_keywords, [Types::PopularKeywordType], null: true
    def get_popular_keywords
      PopularKeyword.popular_keywords
    end

    field :get_interesting_mangas, Types::MangaType.connection_type, null: true
    def get_interesting_mangas
      Manga.interesting_mangas
    end

    field :get_recent_reviews, [Types::ReviewType], null: true
    def get_recent_reviews
      Review.where(netabare: false).order(created_at: :desc).limit(10)
    end

    field :get_popular_users, [Types::UserType], null: true
    def get_popular_users
      User.popular_users(context[:current_user][:user]&.id || 0)
    end

    field :search_manga_titles, [Types::MangaType], null: true do
      argument :word, String, required: true
    end
    def search_manga_titles(word:)
      Manga.title_search(word.to_hira).limit(5)
    end

    field :search_creators, [Types::CreatorType], null: true do
      argument :word, String, required: true
    end
    def search_creators(word:)
      Creator.creator_search(word.to_hira).limit(5)
    end

    field :get_manga_ranking, Types::MangaType.connection_type, null: true do
      argument :type, String, required: true
      argument :genre, String, required: false
    end
    def get_manga_ranking(**args)
      Manga.ranking_by_condition(args[:type], genre: args[:genre])
    end

    field :get_all_questions_by_condition, Types::QuestionType.connection_type, null: true do
      argument :condition, String, required: true
      argument :genre, String, required: false
    end
    def get_all_questions_by_condition(**args)
      Question.all_by_condition(args[:condition], genre: args[:genre])
    end

    field :get_question, Types::QuestionType, null: false do
      argument :id, ID, required: true
    end
    def get_question(id:)
      Question.find(id)
    end

    field :get_show_question_answers, Types::AnswerType.connection_type, null: true do
      argument :id, ID, required: true
    end
    def get_show_question_answers(id:)
      Answer.where(question_id: id).order(updated_at: :desc)
    end

    field :get_notifications, Types::NotificationType.connection_type, null: true
    def get_notifications
      Notification.user_unchecked(context[:current_user][:user])
    end
  end
end
