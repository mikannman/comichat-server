require "json"
require "net/https"
require "uri"

module Types
  class BaseObject < GraphQL::Schema::Object
    field_class Types::BaseField
    connection_type_class Types::CustomConnectionType
  end
end
