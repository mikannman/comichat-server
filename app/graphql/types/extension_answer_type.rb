module Types
  class ExtensionAnswerType < Types::BaseObject
    field :value, [Types::AnswerType], null: true
    field :count, Integer, null: false
  end
end
