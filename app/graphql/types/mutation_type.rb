module Types
  class MutationType < Types::BaseObject
    field :checked_notifications, mutation: Mutations::CheckedNotifications
    
    field :rating_manga, mutation: Mutations::MangaMutation::RatingManga
    field :add_image_url_and_affiliate_url, mutation: Mutations::MangaMutation::AddImageUrlAndAffiliateUrl

    field :delete_user, mutation: Mutations::UserMutation::DeleteUser
    field :update_email, mutation: Mutations::UserMutation::UpdateEmail
    field :update_user, mutation: Mutations::UserMutation::UpdateUser

    field :verify_email, mutation: Mutations::AuthMutation::VerifyEmail

    field :dislikes, mutation: Mutations::LikeMutation::Dislikes
    field :likes, mutation: Mutations::LikeMutation::Likes

    field :delete_reply, mutation: Mutations::ReplyMutation::DeleteReply
    field :update_reply, mutation: Mutations::ReplyMutation::UpdateReply
    field :create_reply, mutation: Mutations::ReplyMutation::CreateReply

    field :delete_answer, mutation: Mutations::AnswerMutation::DeleteAnswer
    field :update_answer, mutation: Mutations::AnswerMutation::UpdateAnswer
    field :create_answer, mutation: Mutations::AnswerMutation::CreateAnswer

    field :delete_question, mutation: Mutations::QuestionMutation::DeleteQuestion
    field :update_question, mutation: Mutations::QuestionMutation::UpdateQuestion
    field :create_question, mutation: Mutations::QuestionMutation::CreateQuestion

    field :delete_review, mutation: Mutations::ReviewMutation::DeleteReview
    field :update_review, mutation: Mutations::ReviewMutation::UpdateReview
    field :create_review, mutation: Mutations::ReviewMutation::CreateReview

    field :unregister_readed_manga, mutation: Mutations::UnregisterReadedManga
    field :register_readed_manga, mutation: Mutations::RegisterReadedManga
    field :unregister_wanna_read_manga, mutation: Mutations::UnregisterWannaReadManga
    field :register_wanna_read_manga, mutation: Mutations::RegisterWannaReadManga
    field :unfollow, mutation: Mutations::Unfollow
    field :follow, mutation: Mutations::Follow
    field :sign_up_or_in, mutation: Mutations::SignUpOrIn
    field :add_popular_keyword, mutation: Mutations::AddPopularKeyword
  end
end
