module Mutations
  class ReviewMutation::DeleteReview < BaseMutation
    argument :id, ID, required: true

    def resolve(**args)
      if context[:current_user][:user]
        Review.find(args[:id]).destroy!

        {
          success: true,
        }
      else
        GraphQL::ExecutionError.new("ログインしてください")
      end
    end
  end
end
