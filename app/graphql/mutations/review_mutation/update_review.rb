module Mutations
  class ReviewMutation::UpdateReview < BaseMutation
    argument :id, ID, required: true
    argument :star, Integer, required: false
    argument :comment, String, required: false
    argument :netabare, Boolean, required: false

    field :review, Types::ReviewType, null: false

    def resolve(**args)
      if context[:current_user][:user]
        Review.find(args[:id]).update!(
          star: args[:star],
          comment: args[:comment],
          netabare: args[:netabare]
        )

        {
          review: Review.find(args[:id]),
        }
      else
        GraphQL::ExecutionError.new("ログインしてください")
      end
    end
  end
end
