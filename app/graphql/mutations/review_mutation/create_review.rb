module Mutations
  class ReviewMutation::CreateReview < BaseMutation
    argument :mid, String, required: true
    argument :star, Integer, required: true
    argument :comment, String, required: false
    argument :netabare, Boolean, required: false

    field :review, Types::ReviewType, null: false

    def resolve(**args)
      if context[:current_user][:user]
        review = context[:current_user][:user].reviews.create(
          mid: args[:mid],
          star: args[:star],
          comment: args[:comment],
          netabare: args[:netabare]
        )

        {
          review: review,
        }
      else
        GraphQL::ExecutionError.new("ログインしてください")
      end
    end
  end
end
