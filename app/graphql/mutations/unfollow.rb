module Mutations
  class Unfollow < BaseMutation
    argument :other_id, ID, required: true

    def resolve(**args)
      if context[:current_user][:user]
        user = context[:current_user][:user]
        other_user = user.active_relationships.find_by(followed_id: args[:other_id]).followed

        user.unfollow(other_user)

        {
          success: true,
        }
      else
        GraphQL::ExecutionError.new("ログインしてください")
      end
    end
  end
end
