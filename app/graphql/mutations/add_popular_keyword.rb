module Mutations
  class AddPopularKeyword < BaseMutation
    argument :word, String, required: true

    def resolve(**args)
      PopularKeyword.create(word: args[:word])

      {
        success: true,
      }
    end
  end
end
