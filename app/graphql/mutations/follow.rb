module Mutations
  class Follow < BaseMutation
    argument :other_id, ID, required: true

    def resolve(**args)
      if context[:current_user][:user]
        user = context[:current_user][:user]
        other_user = User.find(args[:other_id])

        user.follow(other_user)
        user.create_follow_notification(other_user)

        {
          success: true,
        }
      else
        GraphQL::ExecutionError.new("ログインしてください")
      end
    end
  end
end
