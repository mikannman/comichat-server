module Mutations
  class CheckedNotifications < BaseMutation
    argument :ids, [ID], required: false

    field :checked_ids, [ID], null: true

    def resolve(**args)
      Notification.where(id: args[:ids]).update_all(checked: true)

      {
        checked_ids: args[:ids],
      }
    end
  end
end
