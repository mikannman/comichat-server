module Mutations
  class QuestionMutation::CreateQuestion < BaseMutation
    argument :asked_user_id, Integer, required: false
    argument :genres, [String], required: false
    argument :reading_manga_mid, String, required: false
    argument :readed_manga_mid, String, required: false
    argument :content, String, required: false
    argument :all, Boolean, required: false

    field :question, Types::QuestionType, null: false

    def resolve(**args)
      if context[:current_user][:user]
        question = context[:current_user][:user].ask_questions.create(
          asked_user_id: args[:asked_user_id],
          reading_manga_mid: args[:reading_manga_mid],
          readed_manga_mid: args[:readed_manga_mid],
          content: args[:content],
          all: args[:all],
        )
        if args[:genres].present?
          question.add_genre(args[:genres])
        end
        if args[:asked_user_id]
          question.create_quesiton_notification(context[:current_user][:user], args[:asked_user_id])
        end

        {
          question: question,
        }
      else
        GraphQL::ExecutionError.new("ログインしてください")
      end
    end
  end
end
