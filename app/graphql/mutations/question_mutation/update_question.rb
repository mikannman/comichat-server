module Mutations
  class QuestionMutation::UpdateQuestion < BaseMutation
    argument :id, ID, required: true
    argument :asked_user_id, Integer, required: false
    argument :genres, [String], required: false
    argument :reading_manga_mid, String, required: false
    argument :readed_manga_mid, String, required: false
    argument :content, String, required: false
    argument :all, Boolean, required: false

    field :question, Types::QuestionType, null: false

    def resolve(**args)
      if context[:current_user][:user]
        Question.find(args[:id]).update(
          asked_user_id: args[:asked_user_id],
          reading_manga_mid: args[:reading_manga_mid],
          readed_manga_mid: args[:readed_manga_mid],
          content: args[:content],
          all: args[:all],
        )
        updated_question = Question.find(args[:id])
        if args[:genres].present?
          updated_question.change_genre(args[:genres])
        end

        {
          question: updated_question,
        }
      else
        GraphQL::ExecutionError.new("ログインしてください")
      end
    end
  end
end
