module Mutations
  class LikeMutation::Dislikes < BaseMutation
    argument :id, ID, required: true
    argument :type, String, required: true

    def resolve(**args)
      if context[:current_user][:user]
        user = context[:current_user][:user]

        case args[:type]
        when "Answer"
          answer = Answer.find(args[:id])
          user.dislikes_answer(answer)
        when "Reply"
          reply = Reply.find(args[:id])
          user.dislikes_reply(reply)
        when "Review"
          review = Review.find(args[:id])
          user.dislikes_review(review)
        else
          return GraphQL::ExecutionError.new("無効なタイプです")
        end

        {
          success: true,
        }
      else
        GraphQL::ExecutionError.new("ログインしてください")
      end
    end
  end
end
