module Mutations
  class LikeMutation::Likes < BaseMutation
    argument :id, ID, required: true
    argument :type, String, required: true

    def resolve(**args)
      if context[:current_user][:user]
        user = context[:current_user][:user]

        case args[:type]
        when "Answer"
          answer = Answer.find(args[:id])
          user.likes_answer(answer)
          answer.create_like_notifications(user)
        when "Reply"
          reply = Reply.find(args[:id])
          user.likes_reply(reply)
          reply.create_like_notifications(user)
        when "Review"
          review = Review.find(args[:id])
          user.likes_review(review)
          review.create_like_notifications(user)
        else
          return GraphQL::ExecutionError.new("無効なタイプです")
        end

        {
          success: true,
        }
      else
        GraphQL::ExecutionError.new("ログインしてください")
      end
    end
  end
end
