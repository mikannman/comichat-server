module Mutations
  class UnregisterWannaReadManga < BaseMutation
    argument :mid, String, required: true

    def resolve(**args)
      if context[:current_user][:user]
        user = context[:current_user][:user]
        manga = Manga.find_by(mid: args[:mid])

        user.unregister_wanna_read_manga(manga)

        {
          success: true,
        }
      else
        GraphQL::ExecutionError.new("ログインしてください")
      end
    end
  end
end
