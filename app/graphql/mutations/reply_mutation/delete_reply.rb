module Mutations
  class ReplyMutation::DeleteReply < BaseMutation
    argument :id, ID, required: true

    def resolve(**args)
      if context[:current_user][:user]
        Reply.find(args[:id]).destroy!

        {
          success: true,
        }
      else
        GraphQL::ExecutionError.new("ログインしてください")
      end
    end
  end
end
