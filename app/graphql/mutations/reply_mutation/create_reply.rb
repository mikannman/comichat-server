module Mutations
  class ReplyMutation::CreateReply < BaseMutation
    argument :answer_id, ID, required: true
    argument :answer_user_id, ID, required: true
    argument :content, String, required: false

    field :reply, Types::ReplyType, null: false

    def resolve(**args)
      if context[:current_user][:user]
        reply = context[:current_user][:user].replies.create!(
          answer_id: args[:answer_id],
          content: args[:content]
        )
        reply.create_reply_notification(context[:current_user][:user], args[:answer_user_id])

        {
          reply: reply,
        }
      else
        GraphQL::ExecutionError.new("ログインしてください")
      end
    end
  end
end
