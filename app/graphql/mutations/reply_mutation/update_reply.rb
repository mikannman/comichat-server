module Mutations
  class ReplyMutation::UpdateReply < BaseMutation
    argument :id, ID, required: true
    argument :content, String, required: false

    field :reply, Types::ReplyType, null: false

    def resolve(**args)
      if context[:current_user][:user]
        Reply.find(args[:id]).update!(
          content: args[:content]
        )

        {
          reply: Reply.find(args[:id]),
        }
      else
        GraphQL::ExecutionError.new("ログインしてください")
      end
    end
  end
end
