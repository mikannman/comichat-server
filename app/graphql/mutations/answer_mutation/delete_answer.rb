module Mutations
  class AnswerMutation::DeleteAnswer < BaseMutation
    argument :id, ID, required: true

    def resolve(**args)
      if context[:current_user][:user]
        Answer.find(args[:id]).destroy!

        {
          success: true,
        }
      else
        GraphQL::ExecutionError.new("ログインしてください")
      end
    end
  end
end
