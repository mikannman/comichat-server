module Mutations
  class AnswerMutation::CreateAnswer < BaseMutation
    argument :question_id, ID, required: true
    argument :question_user_id, ID, required: true
    argument :content, String, required: true

    field :answer, Types::AnswerType, null: false

    def resolve(**args)
      if context[:current_user][:user]
        answer = context[:current_user][:user].answers.create(
          question_id: args[:question_id],
          content: args[:content]
        )
        answer.create_answer_notification(context[:current_user][:user], args[:question_user_id])

        {
          answer: answer,
        }
      else
        GraphQL::ExecutionError.new("ログインしてください")
      end
    end
  end
end
