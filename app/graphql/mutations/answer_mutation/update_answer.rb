module Mutations
  class AnswerMutation::UpdateAnswer < BaseMutation
    argument :id, ID, required: true
    argument :content, String, required: false

    field :answer, Types::AnswerType, null: false

    def resolve(**args)
      if context[:current_user][:user]
        Answer.find(args[:id]).update!(
          content: args[:content]
        )

        {
          answer: Answer.find(args[:id]),
        }
      else
        GraphQL::ExecutionError.new("ログインしてください")
      end
    end
  end
end
