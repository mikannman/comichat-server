module Mutations
  class UserMutation::DeleteUser < BaseMutation
    def resolve()
      if context[:current_user][:user]
        context[:current_user][:user].destroy

        {
          success: true,
        }
      else
        GraphQL::ExecutionError.new("ログインしてください")
      end
    end
  end
end
