module Mutations
  class UserMutation::UpdateUser < BaseMutation
    argument :user_id, String, required: true
    argument :name, String, required: true
    argument :imageURL, String, required: false
    argument :message, String, required: false

    field :user, Types::UserType, null: false

    def resolve(**args)
      if context[:current_user][:user]
        context[:current_user][:user].update(
          user_id: args[:user_id],
          name: args[:name],
          imageURL: args[:imageURL],
          message: args[:message]
        )

        {
          user: User.find(context[:current_user][:user].id),
        }
      else
        GraphQL::ExecutionError.new("ログインしてください")
      end
    end
  end
end
