module Mutations
  class UserMutation::UpdateEmail < BaseMutation
    argument :email, String, required: true

    field :user, Types::UserType, null: false

    def resolve(**args)
      if context[:current_user][:user]
        context[:current_user][:user].update(
          email: args[:email],
        )

        {
          user: User.find(context[:current_user][:user].id),
        }
      else
        GraphQL::ExecutionError.new("ログインしてください")
      end
    end
  end
end
