module Mutations
  class MangaMutation::AddImageUrlAndAffiliateUrl < BaseMutation
    argument :mid, String, required: true
    argument :image_url, String, required: false
    argument :affiliate_url, String, required: false

    def resolve(**args)
      Manga.find(args[:mid]).update(
        image_url: args[:image_url],
        affiliate_url: args[:affiliate_url]
      )

      {
        success: true,
      }
    end
  end
end
