module Mutations
  class MangaMutation::RatingManga < BaseMutation
    argument :mid, String, required: true
    argument :rate, String, required: true

    field :rating, Types::RatingType, null: false

    def resolve(**args)
      if context[:current_user][:user]
        rating = context[:current_user][:user].rating_manga(args[:mid], args[:rate])

        {
          rating: rating,
        }
      else
        GraphQL::ExecutionError.new("ログインしてください")
      end
    end
  end
end
