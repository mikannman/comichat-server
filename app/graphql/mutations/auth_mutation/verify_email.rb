module Mutations
  class AuthMutation::VerifyEmail < BaseMutation
    field :registered, Boolean, null: false
    field :user_id, String, null: false

    argument :email, String, required: true

    def resolve(**args)
      user_id = args[:email].gsub(/@.*/, "")

      exist_email = User.find_by(email: args[:email])

      if exist_email.nil?
        exist_user_id = User.where("user_id LIKE ?", "%#{user_id}%").order(id: :desc).limit(1)

        if exist_user_id.present?
          user_id += "_#{exist_user_id.first.id}"
        end

        if user_id.length <= 3
          user_id += (0...3).map { ("a".."z").to_a[rand(26)] }.join
        end
      end

      {
        registered: !exist_email.nil?,
        user_id: user_id,
      }
    end
  end
end
