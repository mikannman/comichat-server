module Mutations
  class SignUpOrIn < BaseMutation
    argument :name, String, required: false
    argument :user_id, String, required: false
    argument :email, String, required: true
    argument :imageURL, String, required: false
    argument :provider, String, required: true

    field :user, Types::UserType, null: false

    def resolve(**args)
      if context[:current_user][:error].present?
        GraphQL::ExecutionError.new(
          "ログインに失敗しました。しばらく時間をおいてもう一度お願いします。"
        )
      else
        user = context[:current_user][:user]

        if user.nil?
          user_id = args[:user_id]
          if user_id.blank?
            user_id = args[:email].gsub(/@.*/, "")

            exist_user_id = User.where("user_id LIKE ?", "%#{user_id}%").order(id: :desc).limit(1)

            if exist_user_id.present?
              user_id += "_#{exist_user_id.first.id}"
            end

            if user_id.length <= 3
              user_id += (0...3).map { ("a".."z").to_a[rand(26)] }.join
            end
          end

          if context[:current_user][:guest]
            user = User.create!(
              name: args[:name],
              user_id: SecureRandom.hex,
              email: "#{SecureRandom.hex}@example.com",
              guest: true,
              imageURL: args[:imageURL],
              uid: context[:current_user][:uid],
              provider: args[:provider],
            )
          else
            user = User.create!(
              name: args[:name],
              user_id: user_id,
              email: args[:email],
              guest: false,
              hira: args[:name].to_hira,
              imageURL: args[:imageURL],
              uid: context[:current_user][:uid],
              provider: args[:provider],
            )
          end
        end

        {
          user: user,
        }
      end
    end
  end
end
