module Queries
  class TaughtQuery::GetUnansweredList < BaseQuery
    type Types::QuestionType.connection_type, null: true
    description "未回答の質問を全件取得"

    argument :id, ID, required: true

    def resolve(**args)
      Question.unanswered_list(args[:id])
    end
  end
end
