module Queries
  class TaughtQuery::GetAnsweredList < BaseQuery
    type Types::QuestionType.connection_type, null: true
    description "回答済みの質問を全件取得"

    argument :id, ID, required: true

    def resolve(**args)
      Question.answered_list(args[:id])
    end
  end
end
