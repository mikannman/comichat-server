module Queries
  class TaughtQuery::GetQuestionAnswers < BaseQuery
    type Types::AnswerType.connection_type, null: true
    description "質問の回答を取得"

    argument :id, ID, required: true

    def resolve(**args)
      Answer.all.where(question_id: args[:id]).order(updated_at: :desc)
    end
  end
end
