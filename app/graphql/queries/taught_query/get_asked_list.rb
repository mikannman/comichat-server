module Queries
  class TaughtQuery::GetAskedList < BaseQuery
    type Types::QuestionType.connection_type, null: true
    description "ユーザーの質問を全件取得"

    argument :id, ID, required: true

    def resolve(**args)
      Question.user_asked(args[:id])
    end
  end
end
