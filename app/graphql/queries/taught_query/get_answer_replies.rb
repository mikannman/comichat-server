module Queries
  class TaughtQuery::GetAnswerReplies < BaseQuery
    type Types::ReplyType.connection_type, null: false
    description "回答の返信を取得"

    argument :id, ID, required: true

    def resolve(**args)
      Reply.all.where(answer_id: args[:id]).order(updated_at: :desc)
    end
  end
end
