module Queries
  class MangaQuery::GetReviews < BaseQuery
    type Types::ReviewType.connection_type, null: false
    description "漫画のレビューを取得"
    argument :mid, String, required: true, description: "漫画ID"

    def resolve(**args)
      Review.includes(:user).where(mid: args[:mid]).order(updated_at: "DESC")
    end
  end
end
