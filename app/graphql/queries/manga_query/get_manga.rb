module Queries
  class MangaQuery::GetManga < BaseQuery
    type Types::MangaType, null: false
    description "漫画を一件取得"

    argument :mid, String, required: true, description: "漫画ID"

    def resolve(mid:)
      Manga.find(mid)
    end
  end
end
