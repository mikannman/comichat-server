module Queries
  class MangaQuery::AverageReviews < BaseQuery
    type Float, null: false
    description "漫画のレビューの平均を取得"

    argument :mid, String, required: true, description: "漫画ID"

    def resolve(mid:)
      Review.where(mid: mid).average(:star).round(1) || 0
    end
  end
end
