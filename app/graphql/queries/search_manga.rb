module Queries
  class SearchManga < BaseQuery
    type Types::MangaType.connection_type, null: false
    description "漫画を検索"

    argument :word, String, required: true, description: "検索ワード"
    argument :condition, String, required: false, description: "検索条件"
    argument :selected_list, [String], required: false, description: "選択済みリスト"

    def resolve(**args)
      case args[:condition]
      when "作品名検索"
        Manga.title_search(args[:word].to_hira).distinct
      when "作者名検索"
        Manga.creator_search(args[:word].to_hira).distinct
      when "作者名一覧"
        Manga.by_creator(args[:word]).distinct
      when "ジャンル一覧"
        Manga.by_genre(args[:word]).distinct
      when "出版社一覧"
        Manga.by_publisher(args[:word]).distinct
      when "レーベル一覧"
        Manga.by_label(args[:word]).distinct
      else
        Manga.not_selected(args[:selected_list]).title_search(args[:word].to_hira).distinct
      end
    end
  end
end
