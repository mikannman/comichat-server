module Queries
  class GetUser < BaseQuery
    type Types::UserType, null: false
    description "ユーザーを一件取得"

    argument :id, ID, required: false, description: "ユーザーID"

    def resolve(id:)
      User.find(id)
    end
  end
end
