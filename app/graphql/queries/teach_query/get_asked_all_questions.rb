module Queries
  class TeachQuery::GetAskedAllQuestions < BaseQuery
    type Types::QuestionType.connection_type, null: true
    description "みんなへの質問を全件取得"

    argument :id, ID, required: true

    def resolve(**args)
      Question.asked_all(args[:id])
    end
  end
end
