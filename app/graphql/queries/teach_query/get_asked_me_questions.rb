module Queries
  class TeachQuery::GetAskedMeQuestions < BaseQuery
    type Types::QuestionType.connection_type, null: true
    description "ユーザーへの質問を全件取得"

    argument :id, ID, required: true

    def resolve(**args)
      Question.asked_me(args[:id])
    end
  end
end
