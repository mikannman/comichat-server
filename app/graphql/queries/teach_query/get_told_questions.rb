module Queries
  class TeachQuery::GetToldQuestions < BaseQuery
    type Types::QuestionType.connection_type, null: true
    description "答えた質問を全件取得"

    argument :id, ID, required: true

    def resolve(**args)
      Question.user_told(args[:id])
    end
  end
end
