module Queries
  class VerifyEmail < BaseQuery
    type Types::UserType, null: false
    description "メールアドレスが登録済みかの確認"

    argument :email, String, required: false, description: "メールアドレス"

    def resolve(email:)
      User.find_by(email: email)
    end
  end
end
