module Queries
  class SearchUser < BaseQuery
    type [Types::UserType], null: false
    description "ユーザーを検索"

    argument :word, String, required: true, description: "検索ワード"
    argument :selected_user_id, String, required: false, description: "選択済みユーザー"
    def resolve(**args)
      selected = [context[:current_user][:user]&.user_id, args[:selected_user_id]]
      User.where.not(user_id: selected).search(args[:word]).limit(5)
    end
  end
end
