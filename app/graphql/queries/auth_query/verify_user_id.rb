module Queries
  class AuthQuery::VerifyUserId < BaseQuery
    type Types::VerifyUserIdType, null: false

    description "ユーザーIDの確認"

    argument :user_id, String, required: true

    def resolve(**args)
      if args[:user_id].length <= 3
        { type: "red", text: "短すぎます" }
      elsif args[:user_id].length >= 20
        { type: "red", text: "長すぎます" }
      elsif !User.find_by(user_id: args[:user_id]).nil? &&
        context[:current_user][:user].user_id != args[:user_id]
        { type: "red", text: "すでに登録されています" }
      else
        { type: "green", text: "使用可能です" }
      end
    end
  end
end
