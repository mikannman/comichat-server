module Queries
  class UserQuery::GetFollowers < BaseQuery
    type Types::UserType.connection_type, null: true
    description "フォロワーを全件取得"

    argument :id, ID, required: true

    def resolve(**args)
      User.find(args[:id]).followers.order("relationships.created_at desc")
    end
  end
end
