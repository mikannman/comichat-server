module Queries
  class UserQuery::GetFollowing < BaseQuery
    type Types::UserType.connection_type, null: true
    description "フォローしているユーザーを全件取得"

    argument :id, ID, required: true

    def resolve(**args)
      User.find(args[:id]).following.order("relationships.created_at desc")
    end
  end
end
