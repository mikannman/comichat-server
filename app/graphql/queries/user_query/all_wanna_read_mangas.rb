module Queries
  class UserQuery::AllWannaReadMangas < BaseQuery
    type Types::MangaType.connection_type, null: true
    description "読みたい漫画を全件取得"

    argument :id, ID, required: false, description: "ユーザーID"

    def resolve(**args)
      user = User.find(args[:id])
      user.wanna_read_mangas.order("wanna_reads.created_at desc")
    end
  end
end
