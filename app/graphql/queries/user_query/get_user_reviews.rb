module Queries
  class UserQuery::GetUserReviews < BaseQuery
    type Types::ReviewType.connection_type, null: true
    description "ユーザーのレビューを全件取得"

    argument :id, ID, required: true

    def resolve(**args)
      User.find(args[:id]).reviews.order("reviews.created_at desc")
    rescue => e
      GraphQL::ExecutionError.new(e.message)
    end
  end
end
