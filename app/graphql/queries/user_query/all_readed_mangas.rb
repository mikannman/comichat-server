module Queries
  class UserQuery::AllReadedMangas < BaseQuery
    type Types::MangaType.connection_type, null: true
    description "読んだ漫画を全件取得"

    argument :id, ID, required: true, description: "ユーザーID"

    def resolve(**args)
      user = User.find(args[:id])
      user.readed_mangas.order("readeds.created_at desc")
    end
  end
end
