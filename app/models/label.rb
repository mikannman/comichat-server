class Label < ApplicationRecord
  self.primary_key = :lid
  has_many :manga_labels, dependent: :destroy, foreign_key: "lid"
  has_many :mangas, through: :manga_labels
end
