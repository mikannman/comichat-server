class User < ApplicationRecord
  include SearchCop

  has_many :active_relationships, class_name: "Relationship",
                                  foreign_key: "follower_id", dependent: :destroy
  has_many :passive_relationships, class_name: "Relationship",
                                   foreign_key: "followed_id", dependent: :destroy
  has_many :following, through: :active_relationships, source: :followed
  has_many :followers, through: :passive_relationships, source: :follower
  has_many :wanna_reads, dependent: :destroy
  has_many :readeds, dependent: :destroy
  has_many :reviews, dependent: :destroy
  has_many :wanna_read_mangas, through: :wanna_reads, source: :manga
  has_many :readed_mangas, through: :readeds, source: :manga
  has_many :ask_questions, class_name: "Question",
                           foreign_key: "ask_user_id", dependent: :destroy
  has_many :asked_questions, class_name: "Question",
                             foreign_key: "asked_user_id"
  has_many :answers, dependent: :destroy
  has_many :answered_questions, through: :answers, source: :question
  has_many :replies, dependent: :destroy
  has_many :like_answers, dependent: :destroy
  has_many :like_replies, dependent: :destroy
  has_many :like_reviews, dependent: :destroy
  has_many :liked_answers, through: :like_answers, source: :answer
  has_many :liked_replies, through: :like_replies, source: :reply
  has_many :liked_reviews, through: :like_reviews, source: :review
  has_many :ratings, dependent: :destroy
  has_many :active_notifications, class_name: "Notification", foreign_key: "visitor_id",
                                  dependent: :destroy
  has_many :passive_notifications, class_name: "Notification", foreign_key: "visited_id",
                                   dependent: :destroy

  validates :name, presence: true

  search_scope :search do
    attributes :name, :hira, :user_id
  end

  scope :recommended_users, ->(id) do
    joins(:readeds).where(readeds: { mid: Readed.where(user_id: id).select(:mid).limit(10) }).
      where.not(id: id).where.not(guest: true).distinct.limit(5).order(:id)
  end

  def self.popular_users(id, limit: 5)
    joins(:reviews).where.not(id: id).group(:id).
      order("count(users.id) desc").select(:id, :name, :imageURL, :user_id, :guest).limit(limit)
  end

  # ユーザーをフォローする
  def follow(other_user)
    following << other_user
  end

  # ユーザーをフォロー解除する
  def unfollow(other_user)
    active_relationships.find_by(followed_id: other_user.id).destroy
  end

  # 現在のユーザーがフォローしていたらtrueを返す
  def following?(other_user)
    following.include?(other_user)
  end

  # 読みたい漫画を登録する
  def register_wanna_read_manga(manga)
    wanna_read_mangas << manga
  end

  # 読みたい漫画を解除する
  def unregister_wanna_read_manga(manga)
    wanna_reads.find_by(mid: manga.mid).destroy
  end

  # 読みたい漫画に登録されているか
  def wanna_read_manga?(manga)
    wanna_read_mangas.include?(manga)
  end

  # 読んだ漫画を登録する
  def register_readed_manga(manga)
    readed_mangas << manga
  end

  # 読んだ漫画を解除する
  def unregister_readed_manga(manga)
    readeds.find_by(mid: manga.mid).destroy
  end

  # 読んだ漫画に登録されているか
  def readed_manga?(manga)
    readed_mangas.include?(manga)
  end

  # 回答済みの質問を返す
  def answerd_ask_questions
    Question.answerd_list(id)
  end

  # 未回答の質問を返す
  def unanswerd_ask_questions
    Question.unanswerd_list(id)
  end

  # 回答にいいねする
  def likes_answer(answer)
    liked_answers << answer
  end

  # 回答のいいねを解除する
  def dislikes_answer(answer)
    like_answers.find_by(answer_id: answer.id).destroy
  end

  # 回答にいいねしているか
  def answer_liked?(answer)
    liked_answers.include?(answer)
  end

  # 返信にいいねする
  def likes_reply(reply)
    liked_replies << reply
  end

  # 返信のいいねを解除する
  def dislikes_reply(reply)
    like_replies.find_by(reply_id: reply.id).destroy
  end

  # 返信にいいねしているか
  def reply_liked?(reply)
    liked_replies.include?(reply)
  end

  # レビューにいいねする
  def likes_review(review)
    liked_reviews << review
  end

  # レビューのいいねを解除する
  def dislikes_review(review)
    like_reviews.find_by(review_id: review.id).destroy
  end

  # レビューにいいねしているか
  def review_liked?(review)
    liked_reviews.include?(review)
  end

  # 漫画を評価する
  def rating_manga(mid, rate)
    exist_rating = Rating.find_by(user_id: id, mid: mid)
    if exist_rating
      exist_rating.update(rate: rate)
    else
      ratings.create(mid: mid, rate: rate)
    end

    Rating.find_by(user_id: id, mid: mid)
  end

  # フォロー通知を作る
  def create_follow_notification(other_user)
    temp = Notification.find_by(
      visitor_id: id,
      visited_id: other_user.id,
      action: "follow"
    )

    unless temp
      active_notifications.create(
        visited_id: other_user.id,
        action: "follow"
      )
    end
  end
end
