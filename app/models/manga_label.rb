class MangaLabel < ApplicationRecord
  belongs_to :manga, optional: true, foreign_key: :mid
  belongs_to :label, optional: :true, foreign_key: :lid
end
