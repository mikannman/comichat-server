class Rating < ApplicationRecord
  belongs_to :user
  belongs_to :manga, foreign_key: :mid

  enum rate: { unrated: 0, bad: 1, normal: 2, good: 3 }
end
