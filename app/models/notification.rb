class Notification < ApplicationRecord
  belongs_to :question, optional: true
  belongs_to :answer, optional: true
  belongs_to :reply, optional: true
  belongs_to :review, optional: true
  belongs_to :visitor, class_name: "User"
  belongs_to :visited, class_name: "User"

  enum action: {
    noaction: 0, answerLike: 1, replyLike: 2, reviewLike: 3,
    answer: 4, reply: 5, follow: 6, question: 7,
  }

  scope :user_unchecked, ->(user) do
    where(visited_id: user&.id, checked: false).order(created_at: :desc)
  end
end
