class WannaRead < ApplicationRecord
  belongs_to :user
  belongs_to :manga, foreign_key: :mid
end
