class Manga < ApplicationRecord
  self.primary_key = :mid
  include SearchCop

  has_many :kanas, dependent: :destroy, foreign_key: "mid"
  has_many :manga_creators, foreign_key: "mid"
  has_many :creators, through: :manga_creators
  has_many :manga_genres, foreign_key: "mid"
  has_many :genres, through: :manga_genres
  has_many :manga_labels, foreign_key: "mid"
  has_many :labels, through: :manga_labels
  has_many :wanna_reads, foreign_key: "mid"
  has_many :readeds, foreign_key: "mid"
  has_many :reviews, foreign_key: "mid"
  has_many :ratings, foreign_key: "mid"

  scope :not_selected, ->(selected_list) do
    joins(:kanas).where.not(kanas: { mid: selected_list })
  end

  search_scope :title_search do
    attributes kanas: ["kanas.kana"]

    # options :kanas, left_wildcard: false
  end

  search_scope :creator_search do
    attributes creators: ["creators.creator", "creators.yomi"]

    # options :creators, left_wildcard: false
  end

  def self.ranking_by_condition(type, genre: "")
    case type
    when "weeklyRating"
      # joins(:ratings).where(ratings: { updated_at: Time.current.all_week }).group(:mid).
      #   order("sum(ratings.rate) desc").select(:mid, :title, :date_published, :publisher, :synopsis)
      left_joins(:ratings).group(:mid).order("coalesce(sum(ratings.rate), 0) desc").
        select(:mid, :title, :date_published, :publisher, :synopsis, :image_url, :affiliate_url)
    when "weeklyReviews"
      # joins(:reviews).where(reviews: { created_at: Time.current.all_week }).group(:mid).
      #   order("count(reviews.id) desc").select(:mid, :title, :date_published, :publisher, :synopsis)
      left_joins(:reviews).group(:mid).order("coalesce(count(reviews.id), 0) desc").
        select(:mid, :title, :date_published, :publisher, :synopsis, :image_url, :affiliate_url)
    when "weeklyByGenre"
      # joins(:genres).where(genres: { genre: genre }).
      #   joins(:reviews).where(reviews: { created_at: Time.current.all_week }).group(:mid).
      #   order("avg(reviews.star) desc").select(:mid, :title, :date_published, :publisher, :synopsis)
      joins(:genres).where(genres: { genre: genre }).left_joins(:reviews).group(:mid).
        order("coalesce(avg(reviews.star), 0) desc").select(:mid, :title, :date_published, :publisher, :synopsis, :image_url, :affiliate_url)
    when "allTime"
      left_joins(:reviews).group(:mid).order("coalesce(avg(reviews.star), 0) desc").
        select(:mid, :title, :date_published, :publisher, :synopsis, :image_url, :affiliate_url)
    end
  end

  scope :by_genre, ->(genre) do
    includes(:genres).where(genres: { genre: genre })
  end

  scope :by_publisher, ->(publisher) do
    where(publisher: publisher)
  end

  scope :by_creator, ->(creator) do
    includes(:creators).where(creators: { creator: creator })
  end

  scope :by_label, ->(label) do
    includes(:labels).where(labels: { label: label })
  end

  scope :interesting_mangas, -> do
    joins(:ratings).where(ratings: { created_at: Time.current.all_week }).group(:mid).
      order("sum(ratings.rate) desc").select(:mid, :title)
  end

  scope :rated_mangas, ->(user_id, rate) do
    joins(:ratings).where(user_id: user_id, rate: rate)
  end
end
