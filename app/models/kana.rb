class Kana < ApplicationRecord
  include SearchCop

  belongs_to :manga, optional: true

  search_scope :search do
    attributes :kana
  end
end
