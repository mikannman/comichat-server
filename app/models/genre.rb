class Genre < ApplicationRecord
  self.primary_key = :gid
  has_many :manga_genres, dependent: :destroy, foreign_key: "gid"
  has_many :mangas, through: :manga_genres
  has_many :question_genres, foreign_key: "gid"
end
