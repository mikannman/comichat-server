class Question < ApplicationRecord
  has_many :notifications, dependent: :destroy
  has_many :answers, dependent: :destroy
  has_many :question_genres, dependent: :destroy
  has_many :genres, through: :question_genres, source: :genre
  belongs_to :ask_user, class_name: "User"
  belongs_to :asked_user, class_name: "User", optional: true

  scope :answered_list, ->(id) do
    where(id: Answer.pluck(:question_id).uniq, ask_user_id: id).
      order(updated_at: :desc)
  end

  scope :unanswered_list, ->(id) do
    where.not(id: Answer.pluck(:question_id).uniq).
      where(ask_user_id: id).order(updated_at: :desc)
  end

  def self.all_by_condition(condition, genre: "")
    case condition
    when "recent"
      where(all: true).order(updated_at: :desc)
    when "answered"
      where(id: Answer.pluck(:question_id).uniq).where(all: true).order(updated_at: :desc)
    when "unanswered"
      where.not(id: Answer.pluck(:question_id).uniq).where(all: true).order(updated_at: :desc)
    when "byGenre"
      includes(:question_genres).where(all: true, question_genres: { gid: genre }).
        order(updated_at: :desc)
    end
  end

  scope :user_asked, ->(id) do
    where(ask_user_id: id).order(updated_at: :desc)
  end

  scope :asked_all, ->(id) do
    where(all: true).where.not(ask_user_id: id).order(updated_at: :desc)
  end

  scope :asked_me, ->(id) do
    where(asked_user_id: id).order(updated_at: :desc)
  end

  scope :user_told, ->(id) do
    includes(:answers).where(answers: { user_id: id }).
      order(updated_at: :desc).distinct
  end

  def add_genre(gids)
    now = Time.zone.now
    question_genres = []
    gids.each do |gid|
      question_genres << {
        question_id: id,
        gid: gid,
        created_at: now,
        updated_at: now,
      }
    end

    QuestionGenre.insert_all(question_genres)
  end

  def change_genre(gids)
    QuestionGenre.where(question_id: id).destroy_all

    add_genre(gids)
  end

  def create_quesiton_notification(current_user, asked_user_id)
    current_user.active_notifications.create(
      visited_id: asked_user_id,
      question_id: id,
      action: "question"
    )
  end
end
