class Creator < ApplicationRecord
  self.primary_key = :cid
  include SearchCop

  has_many :manga_creators, dependent: :destroy, foreign_key: "cid"
  has_many :mangas, through: :manga_creators

  search_scope :creator_search do
    attributes :creator, :yomi

    options :creator, left_wildcard: false
    options :yomi, left_wildcard: false
  end
end
