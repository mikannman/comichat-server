class MangaGenre < ApplicationRecord
  belongs_to :manga, optional: true, foreign_key: :mid
  belongs_to :genre, optional: :true, foreign_key: :gid
end
