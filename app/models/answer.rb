class Answer < ApplicationRecord
  belongs_to :question
  belongs_to :user
  has_many :replies, dependent: :destroy
  has_many :like_answers, dependent: :destroy
  has_many :liked_users, through: :like_answers, source: :user
  has_many :notifications, dependent: :destroy

  def create_like_notifications(current_user)
    unless current_user.id == user_id
      temp = Notification.find_by(
        visitor_id: current_user.id,
        visited_id: user_id,
        answer_id: id,
        action: "answerLike"
      )

      unless temp
        current_user.active_notifications.create(
          visited_id: user_id,
          answer_id: id,
          action: "answerLike"
        )
      end
    end
  end

  def create_answer_notification(current_user, question_user_id)
    unless current_user.id == question_user_id
      current_user.active_notifications.create(
        visited_id: question_user_id,
        answer_id: id,
        action: "answer"
      )
    end
  end
end
