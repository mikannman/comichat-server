class Reply < ApplicationRecord
  belongs_to :answer
  belongs_to :user
  has_many :like_replies, dependent: :destroy
  has_many :liked_users, through: :like_replies, source: :user
  has_many :notifications, dependent: :destroy

  def create_like_notifications(current_user)
    unless current_user.id == user_id
      temp = Notification.find_by(
        visitor_id: current_user.id,
        visited_id: user_id,
        reply_id: id,
        action: "replyLike"
      )

      unless temp
        current_user.active_notifications.create(
          visited_id: user_id,
          reply_id: id,
          action: "replyLike"
        )
      end
    end
  end

  def create_reply_notification(current_user, answer_user_id)
    unless current_user.id == answer_user_id
      current_user.active_notifications.create(
        visited_id: answer_user_id,
        reply_id: id,
        action: "reply"
      )
    end
  end
end
