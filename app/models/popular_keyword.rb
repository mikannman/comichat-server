class PopularKeyword < ApplicationRecord
  scope :popular_keywords, -> do
    where(created_at: Time.current.all_day).group(:word).
      order("count(id) desc").select(:word).limit(20)
  end
end
