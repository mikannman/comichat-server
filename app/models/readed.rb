class Readed < ApplicationRecord
  belongs_to :user
  belongs_to :manga, foreign_key: :mid

  scope :favorite_manga_mid, ->(user_id) do
    where(user_id: user_id).limit(1)
  end
end
