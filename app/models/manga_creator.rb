class MangaCreator < ApplicationRecord
  belongs_to :manga, optional: true, foreign_key: :mid
  belongs_to :creator, optional: :true, foreign_key: :cid
end
