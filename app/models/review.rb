class Review < ApplicationRecord
  belongs_to :user
  belongs_to :manga, foreign_key: :mid
  has_many :like_reviews, dependent: :destroy
  has_many :liked_users, through: :like_reviews, source: :user
  has_many :notifications, dependent: :destroy

  def create_like_notifications(current_user)
    unless current_user.id == user_id
      temp = Notification.find_by(
        visitor_id: current_user.id,
        visited_id: user_id,
        review_id: id,
        action: "reviewLike"
      )

      unless temp
        current_user.active_notifications.create(
          visited_id: user_id,
          review_id: id,
          action: "reviewLike"
        )
      end
    end
  end
end
